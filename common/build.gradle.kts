plugins {
    id("multiplatform-setup")
}

repositories {
    google()
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    api(Deps.Di.Koin.core)
    api(Deps.Kotlin.Coroutines.core)
//    api(Deps.Kotlin.Coroutines.core_jvm)
    api(Deps.Kotlin.serialization)
}

package com.pixelperfect.packagejson.common

import org.koin.core.Koin
import org.koin.core.context.GlobalContext
import org.koin.core.module.Module

fun koinInit(modules: List<Module>) {
    if (GlobalContext.getOrNull() != null) return
    GlobalContext.startKoin {
        allowOverride(true)
        modules(modules)
    }
}

fun getKoin(): Koin? {
    return GlobalContext.getOrNull()
}
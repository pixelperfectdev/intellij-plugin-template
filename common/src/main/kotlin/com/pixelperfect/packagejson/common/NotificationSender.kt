package com.pixelperfect.packagejson.common

interface NotificationSender {

    fun notify(
        title: String,
        message: String,
        iconPath: String,
        actions: List<NotificationActionType>,
        callback: (NotificationActionType) -> Unit
    )
    fun notifySuccess(title: String, message: String)
    fun notifyWarning(title: String, message: String)
    fun notifyError(title: String, message: String)
    fun displayBackgroundProgressIndicator(title: String): Any // BackgroundableProcessIndicator

}

enum class NotificationActionType {
    Restart,
    DisplayLog,
    Cancel
}
package com.pixelperfect.packagejson.common

fun Double.toRatio(min: Double, max: Double): Double {
    return this.div(100) * (max - min) + min
}
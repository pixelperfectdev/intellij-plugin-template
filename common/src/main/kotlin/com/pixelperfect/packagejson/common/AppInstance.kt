package com.pixelperfect.packagejson.common

import org.koin.core.component.KoinScopeComponent
import org.koin.core.component.createScope
import org.koin.core.scope.Scope

class AppInstance: KoinScopeComponent {
    override val scope: Scope by lazy { createScope(this) }
}
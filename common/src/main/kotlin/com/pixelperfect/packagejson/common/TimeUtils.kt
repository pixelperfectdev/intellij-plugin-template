package com.pixelperfect.packagejson.common

import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter.ofLocalizedDateTime
import java.time.format.DateTimeParseException
import java.time.format.FormatStyle

fun String?.formatDate(): String {
    if (this.isNullOrBlank()) return ""
    return try {
        ZonedDateTime.parse(this)
            .withZoneSameInstant(ZoneId.systemDefault())
            .format(ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT))
    } catch (e: DateTimeParseException) {
        this
    } catch (e: Exception) {
        this
    }
}
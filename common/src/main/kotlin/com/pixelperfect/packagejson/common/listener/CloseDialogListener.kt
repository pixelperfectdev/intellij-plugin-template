package com.pixelperfect.packagejson.common.listener

interface CloseDialogListener {
    fun onCloseRequest()
}
package com.pixelperfect.packagejson.common

interface Presenter {
    fun clear()
}
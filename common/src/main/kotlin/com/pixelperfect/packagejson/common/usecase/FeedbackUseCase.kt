package com.pixelperfect.packagejson.common.usecase

interface FeedbackUseCase {
    suspend operator fun invoke()
}
package com.pixelperfect.packagejson.common

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.core.Koin
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import org.koin.dsl.module

object KoinName {

    const val LOG_ROOT_FOLDER = "LOG_ROOT_FOLDER"
    const val DOWNLOAD_FOLDER = "DOWNLOAD_FOLDER"
    const val DISPATCHER_IO = "DISPATCHER_IO"
    const val DISPATCHER_UI = "DISPATCHER_UI"
    const val COROUTINE_IO = "COROUTINE_IO"
    const val COROUTINE_UI = "COROUTINE_UI"
    const val BASIC_HTTP_CLIENT = "BASIC_HTTP_CLIENT"

}

inline fun <reified R> Koin.getAllDistinct(): List<R> = getAll<R>().distinct()

inline fun <reified R> Scope.getAllDistinct(): List<R> = getAll<R>(R::class).distinct()

val commonModule = module {

    // Coroutines

    factory<CoroutineScope>(named(KoinName.COROUTINE_IO)) {
        CoroutineScope(
            SupervisorJob()
                    + get<CoroutineDispatcher>(named(KoinName.DISPATCHER_IO))
                    + get<CoroutineExceptionHandler>()
        )
    }
    factory<CoroutineScope>(named(KoinName.COROUTINE_UI)) {
        CoroutineScope(
            SupervisorJob()
                    + get<CoroutineDispatcher>(named(KoinName.DISPATCHER_UI))
                    + get<CoroutineExceptionHandler>()
        )
    }
    factory<CoroutineExceptionHandler> {
        CoroutineExceptionHandler { _, exception ->
            exception.printStackTrace()
        }
    }
    factory<CoroutineDispatcher>(named(KoinName.DISPATCHER_UI)) { Dispatchers.Default }
    factory<CoroutineDispatcher>(named(KoinName.DISPATCHER_IO)) { Dispatchers.Default }
}
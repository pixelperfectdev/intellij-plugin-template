package com.pixelperfect.packagejson.common

import com.pixelperfect.packagejson.common.usecase.FeedbackUseCase
import com.pixelperfect.packagejson.common.usecase.MoreExtensionUseCase
import com.pixelperfect.packagejson.common.listener.CloseDialogListener
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named

abstract class ViewModel : KoinComponent {
    val viewModelScope by inject<CoroutineScope>(named(KoinName.COROUTINE_IO))
    val handler by inject<CoroutineExceptionHandler>()
    private val feedbackUserCase: FeedbackUseCase by inject()
    private val moreExtensionUserCase: MoreExtensionUseCase by inject()

    fun onHelpClick() {
        viewModelScope.launch { feedbackUserCase() }
    }

    fun onExtensionClickClick() {
        viewModelScope.launch { moreExtensionUserCase() }
    }

    open fun onCleared() {
        viewModelScope.cancel()
    }
}

interface DialogViewModel {

    var closeDialogListener: CloseDialogListener?
    fun init(dashboardId: String, projectId: String)
    fun onClose()

}
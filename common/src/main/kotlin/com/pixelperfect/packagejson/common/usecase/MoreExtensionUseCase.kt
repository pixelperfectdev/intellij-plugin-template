package com.pixelperfect.packagejson.common.usecase

interface MoreExtensionUseCase {
    suspend operator fun invoke()
}
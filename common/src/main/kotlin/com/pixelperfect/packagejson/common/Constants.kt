package com.pixelperfect.packagejson.common

object Constants {
    const val CONTENT_VIEW_ID = "contentViewExtension"
    const val CONTAINER_ID = "contentViewContainer"
    const val DIALOG_ID = "contentViewDialog"
}
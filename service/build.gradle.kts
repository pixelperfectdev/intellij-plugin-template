plugins {
    id("multiplatform-setup")
}

dependencies {
    implementation(Deps.Kotlin.serialization)
    implementation(project(":repository"))
    implementation(project(":common"))
    implementation(project(":data"))

    // Cache
    implementation(Deps.Storage.Cache.settings)

    // Ktor client
    implementation(Deps.Network.Ktor.json)
    implementation(Deps.Network.Ktor.client_core)
    implementation(Deps.Network.Ktor.client_json)
    implementation(Deps.Network.Ktor.client_auth)
    implementation(Deps.Network.Ktor.client_okhttp)
    implementation(Deps.Network.Ktor.client_serialization)
    implementation(Deps.Network.Ktor.client_content_negotiation)

    implementation(Deps.Kotlin.Coroutines.core)
//    implementation(Deps.Kotlin.Coroutines.core_jvm)

    implementation(Deps.Network.Logger.log4j)
}
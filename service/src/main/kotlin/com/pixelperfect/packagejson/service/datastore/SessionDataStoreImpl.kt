package com.pixelperfect.packagejson.service.datastore

import com.pixelperfect.packagejson.repository.service.datastore.SessionDataStore

class SessionDataStoreImpl: SessionDataStore {

    override suspend fun setDarkTheme(enabled: Boolean) {
    }

    override suspend fun isDarkThemeEnabled(): Boolean = false

    override fun clear() {
    }
}
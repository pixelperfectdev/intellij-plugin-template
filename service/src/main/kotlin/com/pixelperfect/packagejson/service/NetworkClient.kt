package com.pixelperfect.packagejson.service

import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

object NetworkClient {
    fun httpClient(
        customHeaders: Map<String, String> = emptyMap()
    ): HttpClient = HttpClient(OkHttp) {
        install(DefaultRequest)
        install(HttpTimeout) {
            socketTimeoutMillis = 30_000
            requestTimeoutMillis = 30_000
            connectTimeoutMillis = 30_000
        }
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                ignoreUnknownKeys = true
            })
        }
        engine {
            config {
                sslSocketFactory(
                    SslSettings.getSslContext()!!.socketFactory,
                    SslSettings.getTrustManager()
                )
            }
        }
        defaultRequest {
            customHeaders.forEach { (key, value) ->
                header(key, value)
            }
            header("Content-Type", "application/json")
            contentType(ContentType.Application.Json)
            accept(ContentType.Application.Json)
        }
    }
}

class TrustAllX509TrustManager : X509TrustManager {
    override fun getAcceptedIssuers(): Array<X509Certificate?> = arrayOfNulls(0)

    override fun checkClientTrusted(certs: Array<X509Certificate?>?, authType: String?) {}

    override fun checkServerTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
}

object SslSettings {
    fun getSslContext(): SSLContext? {
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, arrayOf(TrustAllX509TrustManager()), null)
        return sslContext
    }

    fun getTrustManager(): X509TrustManager {
        return TrustAllX509TrustManager()
    }
}
package com.pixelperfect.packagejson.service.di

import com.pixelperfect.packagejson.common.KoinName
import com.pixelperfect.packagejson.repository.service.datasource.NpmDataSource
import com.pixelperfect.packagejson.service.datasource.NpmDataSourceImpl
import org.koin.core.qualifier.named
import org.koin.dsl.module

val dataSourceModule = module {

    single<NpmDataSource> {
        NpmDataSourceImpl(
            httpClient = get(),
            coroutineScope = get(named(KoinName.COROUTINE_IO)),
        )
    }
}
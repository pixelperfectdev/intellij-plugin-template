package com.pixelperfect.packagejson.service.di

import com.pixelperfect.packagejson.service.NetworkClient
import io.ktor.client.*
import org.koin.dsl.module

val serviceModule = module {

    single<HttpClient> {
        NetworkClient.httpClient()
    }

    includes(dataStoreModule, dataSourceModule)
}
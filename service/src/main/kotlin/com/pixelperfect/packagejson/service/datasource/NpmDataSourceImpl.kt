package com.pixelperfect.packagejson.service.datasource

import com.pixelperfect.packagejson.common.NOT_IMPLEMENTED
import com.pixelperfect.packagejson.common.runCatchingCancellable
import com.pixelperfect.packagejson.data.npm.*
import com.pixelperfect.packagejson.repository.service.datasource.NpmDataSource
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import kotlinx.coroutines.CoroutineScope
import okhttp3.internal.toImmutableMap

class NpmDataSourceImpl(
    private val httpClient: HttpClient,
    private val coroutineScope: CoroutineScope,
): NpmDataSource {

    private val apiUrl: String = "https://registry.npmjs.org"

    override suspend fun searchPackage(query: String): Result<List<NpmPackageSearchResult>> {
        return runCatchingCancellable {
            httpClient.get("$apiUrl/-/v1/search") {
                url {
                    parameters.append("text", query)
                    parameters.append("size", "20")
                }
            }.body<NpmSearchResponse>().objects ?: emptyList()
        }
    }

    override suspend fun fetchPackage(packageName: String): Result<NpmPackage> {
        return runCatchingCancellable {
            val response = httpClient.get("$apiUrl/$packageName").body<NpmPackageVersions>()
            if (response.error != null) throw RuntimeException()
            response.versions?.get(response.version) ?: throw RuntimeException()
        }
    }

    override suspend fun fetchPackage(packageName: String, version: String?): Result<NpmPackage> {
        return runCatchingCancellable {
            val cleanedVersion = version.cleanVersion() ?: throw RuntimeException()
            val response = httpClient.get("$apiUrl/$packageName/$cleanedVersion").body<NpmPackage>()
            if (response.error != null) throw RuntimeException()
            response
        }
    }

    override suspend fun auditPackage(npmPackage: NpmPackage): Result<NpmPackageVulnerabilities> {
        return runCatchingCancellable {
            val response = httpClient.post("$apiUrl/-/npm/v1/security/audits") {
                setBody(CheckVulnerabilitiesInput.create(npmPackage))
            }
            response.body<NpmAuditResponse>().let {
                NpmPackageVulnerabilities.create(it)
            }
        }
    }

    override suspend fun checkDependenciesVulnerabilities(npmPackage: NpmPackage): Result<NpmPackageVulnerabilities> {
        return runCatchingCancellable {
            NOT_IMPLEMENTED()
//            val dependencies = getDependencies(npmPackage.name, npmPackage.version)
//
//            val response = httpClient.post("$apiUrl/-/npm/v1/security/advisories/bulk") {
//                setBody(dependencies)
//            }
//            response.body<NpmVulnerabilityBulkResponse>().let {
//                NpmPackageVulnerabilities.create(it)
//            }
        }
    }

//    private suspend fun getDependencies(pkg: String?, version: String?): Map<String, Set<String>> {
//        val dependencies = (fetchPackage(pkg!!, version).getOrNull()?.dependencies ?: emptyMap())
//        val allDependencies = mutableMapOf<String, Set<String>>()
//
//        allDependencies.putAll(dependencies.mapValues { mutableSetOf(it.value) })
//
//        val subDependencies = dependencies.map { (_pkg, _version) ->
//            getDependencies(_pkg, _version)
//        }
//
//        subDependencies.onEach { map ->
//            map.onEach { (k: String, v: Set<String>) ->
//                allDependencies[k] = allDependencies[k]?.let { it + v } ?: v
//            }
//        }
//        return allDependencies
//    }

    override suspend fun getDependencies(dependency: NpmDependency): List<NpmDependency> {
        return (fetchPackage(dependency.name, dependency.version.cleanVersion()).getOrNull()?.dependencies ?: emptyMap())
                .entries.map { NpmDependency(it.key, it.value, emptyList()) }
    }
}
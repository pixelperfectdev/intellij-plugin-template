package com.pixelperfect.packagejson.service.di

import com.pixelperfect.packagejson.repository.service.datastore.SessionDataStore
import com.pixelperfect.packagejson.service.datastore.SessionDataStoreImpl
import org.koin.dsl.module

val dataStoreModule = module {

    factory<SessionDataStore> {
        SessionDataStoreImpl()
    }
}
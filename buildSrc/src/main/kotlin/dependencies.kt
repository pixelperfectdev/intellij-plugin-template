object Deps {

    object Kotlin {
        private const val SERIALIZATION_VERSION = "1.5.0"
        const val serialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:$SERIALIZATION_VERSION"

        object Coroutines {
            private const val VERSION = "1.6.4"
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${VERSION}"
            const val core_jvm = "org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:${VERSION}"
            const val swing = "org.jetbrains.kotlinx:kotlinx-coroutines-swing:${VERSION}"
        }
    }

    object Di {
        object Koin {
            private const val VERSION = "3.3.3"
            const val core = "io.insert-koin:koin-core:$VERSION"
        }
        object CoKoin {
            private const val VERSION = "1.0.0"
            const val core = "dev.burnoo:cokoin:$VERSION"
        }
    }

    object Network {
        object Ktor {
            private const val VERSION = "2.2.3"
            // Client
            const val client_core = "io.ktor:ktor-client-core:$VERSION"
            const val client_okhttp = "io.ktor:ktor-client-okhttp:$VERSION"
            const val client_logging = "io.ktor:ktor-client-logging:$VERSION"
            const val client_json = "io.ktor:ktor-client-json:$VERSION"
            const val client_auth = "io.ktor:ktor-client-auth:$VERSION"
            const val client_serialization = "io.ktor:ktor-client-serialization:$VERSION"
            const val client_content_negotiation = "io.ktor:ktor-client-content-negotiation:$VERSION"
            const val client_cio = "io.ktor:ktor-client-cio:$VERSION"
            // Utils
            const val json = "io.ktor:ktor-serialization-kotlinx-json:$VERSION"
            // Test
            const val client_test = "io.ktor:ktor-client-mock:$VERSION"
        }

        object Logger {
            private const val LOG4J_VERSION = "2.0.7"
            const val log4j = "org.slf4j:slf4j-simple:$LOG4J_VERSION"
        }
    }

    object Storage {
        object Cache {
            private const val CACHE_VERSION = "1.0.0"
            const val settings = "com.russhwolf:multiplatform-settings-no-arg:$CACHE_VERSION"
        }
    }

}
fun properties(key: String) = project.findProperty(key).toString()

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

dependencies {
    compileOnly(compose.ui)
    compileOnly(compose.material)
    compileOnly(compose.runtime)
    compileOnly(compose.foundation)
    compileOnly(compose.desktop.common)
}

import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.util.*

fun getLocalProperty(rootDir: File, key: String, file: String = "local.properties"): Any {
    val properties = Properties()
    val localProperties = File(rootDir, file)
    if (localProperties.isFile) {
        InputStreamReader(FileInputStream(localProperties), Charsets.UTF_8).use { reader ->
            properties.load(reader)
        }
    } else error("File from not found")

    return properties.getProperty(key, "")
}

fun readFile(rootDir: File, file: String): String {
    return File(rootDir, file).readText()
}
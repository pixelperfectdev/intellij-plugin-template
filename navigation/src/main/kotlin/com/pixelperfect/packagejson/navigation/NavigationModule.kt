package com.pixelperfect.packagejson.navigation

import com.pixelperfect.packagejson.common.AppInstance
import com.pixelperfect.packagejson.common.KoinName
import org.koin.core.qualifier.named
import org.koin.dsl.module

val navigationModule = module {

    single<NavigationSessionManager> {
        NavigationSessionManagerImpl()
    }

    scope<AppInstance> {
        scoped<NavigationManager> {
            NavigationManagerImpl(
                externalNavigator = get(),
                navigationSessionManager = get(),
                coroutineScope = get(named(KoinName.COROUTINE_IO))
            )
        }
    }
}
package com.pixelperfect.packagejson.navigation

sealed interface Route {

    object Idle: Route
    object Connect: Route
    object Dashboard: Route
    object Logout: Route
    object ProjectList: Route
    class BuildList(val projectId: String, val projectName: String?): Route
    class SetupVariables(val projectId: String): Route
    class SetupCollaborators(val projectId: String): Route
    class SetupSecureFiles(val projectId: String): Route
    class OpenDockerImage(val imageId: String): Route
    class NewBuild(val projectId: String): Route
    class RefreshBuilds: Route
    class ShowLog(): Route
    object Discount: Route

    class SelectProject: Route
    class CreateDashboard: Route
}
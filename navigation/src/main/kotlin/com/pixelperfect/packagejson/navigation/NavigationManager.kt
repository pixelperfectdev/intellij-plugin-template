package com.pixelperfect.packagejson.navigation

import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow

interface NavigationSessionManager {
    val route: SharedFlow<Route>
    fun navigateToLogout()
    fun navigateToLogin()
    fun refreshBuilds()
}

interface NavigationManager {
    val route: StateFlow<Route>
    fun navigateTo(route: Route)
    fun navigateToLink(url: String)
    fun copyToClipboard(content: String)

}

interface ExternalNavigator {
    fun navigateToLink(url: String)
}

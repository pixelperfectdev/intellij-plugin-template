package com.pixelperfect.packagejson.navigation

import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection

internal fun String.copyToClipboard() {
    val clipboard: Clipboard = Toolkit.getDefaultToolkit().systemClipboard
    val selection = StringSelection(this)
    clipboard.setContents(selection, selection)
}

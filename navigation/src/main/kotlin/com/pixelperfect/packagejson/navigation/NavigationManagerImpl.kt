package com.pixelperfect.packagejson.navigation

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class NavigationManagerImpl(
    private val externalNavigator: ExternalNavigator,
    private val navigationSessionManager: NavigationSessionManager,
    private val coroutineScope: CoroutineScope
): NavigationManager {

    private val _route = MutableStateFlow<Route>(Route.Idle)
    override val route = _route.asStateFlow()

    init {
        coroutineScope.launch {
            navigationSessionManager.route.collect { route ->
                if (route == Route.Logout || route == Route.Dashboard) {
                    _route.value = route
                }
            }
        }
    }

    override fun navigateTo(route: Route) {
        _route.value = route
    }

    override fun navigateToLink(url: String) {
        if (url.isNotEmpty()) externalNavigator.navigateToLink(url)
    }

    override fun copyToClipboard(content: String) {
        content.copyToClipboard()
    }
}

class NavigationSessionManagerImpl: NavigationSessionManager {
    private val _route = MutableStateFlow<Route>(Route.Idle)
    override val route = _route.asSharedFlow()

    override fun navigateToLogout() {
        _route.value = Route.Logout
    }

    override fun navigateToLogin() {
        _route.value = Route.Dashboard
    }

    override fun refreshBuilds() {
        _route.value = Route.RefreshBuilds()
    }
}
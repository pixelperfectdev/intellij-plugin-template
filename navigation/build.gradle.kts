plugins {
    id("multiplatform-setup")
}

dependencies {
    implementation(project(":data"))
    implementation(project(":common"))

    implementation(Deps.Di.Koin.core)
    implementation(Deps.Kotlin.Coroutines.core)
//    implementation(Deps.Kotlin.Coroutines.core_jvm)
    implementation(Deps.Kotlin.serialization)
}
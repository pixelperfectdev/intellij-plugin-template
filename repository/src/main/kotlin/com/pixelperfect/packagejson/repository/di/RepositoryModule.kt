package com.pixelperfect.packagejson.repository.di

import com.pixelperfect.packagejson.common.KoinName
import com.pixelperfect.packagejson.domain.repository.NpmRepository
import com.pixelperfect.packagejson.domain.repository.Repository
import com.pixelperfect.packagejson.domain.repository.SessionRepository
import com.pixelperfect.packagejson.repository.NpmRepositoryImpl
import com.pixelperfect.packagejson.repository.SessionRepositoryImpl
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {

    single<SessionRepository> {
        SessionRepositoryImpl(
            sessionDataStore = get(),
        )
    } bind Repository::class

    single<NpmRepository> {
        NpmRepositoryImpl(
            npmDataSource = get(),
            coroutine = get(named(KoinName.COROUTINE_IO)),
        )
    } bind Repository::class

}
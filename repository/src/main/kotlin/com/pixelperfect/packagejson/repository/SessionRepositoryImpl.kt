package com.pixelperfect.packagejson.repository

import com.pixelperfect.packagejson.domain.repository.SessionRepository
import com.pixelperfect.packagejson.repository.service.datastore.SessionDataStore

class SessionRepositoryImpl(
    private val sessionDataStore: SessionDataStore
): SessionRepository {

    override fun isConnected(): Boolean {
        return false
    }

    override suspend fun setDarkTheme(enabled: Boolean) {
        sessionDataStore.setDarkTheme(enabled)
    }

    override suspend fun isDarkThemeEnabled(): Boolean = sessionDataStore.isDarkThemeEnabled()

    override suspend fun clear() {
        sessionDataStore.clear()
    }
}
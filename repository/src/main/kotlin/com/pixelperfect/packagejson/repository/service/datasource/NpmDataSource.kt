package com.pixelperfect.packagejson.repository.service.datasource

import com.pixelperfect.packagejson.data.npm.NpmDependency
import com.pixelperfect.packagejson.data.npm.NpmPackage
import com.pixelperfect.packagejson.data.npm.NpmPackageSearchResult
import com.pixelperfect.packagejson.data.npm.NpmPackageVulnerabilities

interface NpmDataSource {

    suspend fun searchPackage(query: String): Result<List<NpmPackageSearchResult>>
    suspend fun fetchPackage(packageName: String): Result<NpmPackage>
    suspend fun fetchPackage(packageName: String, version: String?): Result<NpmPackage>
    suspend fun auditPackage(npmPackage: NpmPackage): Result<NpmPackageVulnerabilities>
    suspend fun checkDependenciesVulnerabilities(npmPackage: NpmPackage): Result<NpmPackageVulnerabilities>

    suspend fun getDependencies(dependency: NpmDependency): List<NpmDependency>

}
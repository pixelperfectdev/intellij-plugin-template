package com.pixelperfect.packagejson.repository.service.datastore

import com.pixelperfect.packagejson.data.npm.NpmPackageInfo

interface NpmPackageDataStore: DataStore {

    suspend fun savePackage(pkg: NpmPackageInfo)
    suspend fun getPackage(name: String): Result<NpmPackageInfo>

}
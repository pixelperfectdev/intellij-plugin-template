package com.pixelperfect.packagejson.repository.service.datastore

interface DataStore {
    fun clear()
}
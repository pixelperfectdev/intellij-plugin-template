package com.pixelperfect.packagejson.repository

import com.pixelperfect.packagejson.common.flatMap
import com.pixelperfect.packagejson.data.error.PackageNotFound
import com.pixelperfect.packagejson.data.npm.*
import com.pixelperfect.packagejson.domain.repository.NpmRepository
import com.pixelperfect.packagejson.repository.service.datasource.NpmDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch

class NpmRepositoryImpl(
    private val npmDataSource: NpmDataSource,
    private val coroutine: CoroutineScope
): NpmRepository {

    private val packages = mutableMapOf<String, NpmPackage>()
    private val searchPackages = mutableSetOf<NpmPackageInfo>()
    private val vulnerabilities = mutableMapOf<String, NpmPackageVulnerabilities>()
    private val dependencies = mutableMapOf<String, NpmDependency>()

    //  Get Package

    override fun getPackage(packageName: String, version: String?): Result<NpmPackage> {
        return packages[packageName]
            ?.let { Result.success(it) }
            ?: run {
                coroutine.launch { fetchPackage(packageName) }
                Result.failure(PackageNotFound())
            }
    }

    override suspend fun fetchPackage(packageName: String, version: String?): Result<NpmPackage> {
        return packages[packageName]
            ?.let { Result.success(it) }
            ?: npmDataSource.fetchPackage(packageName)
                .onSuccess { packages.put(it.name, it) }
    }

    // Packages

    override fun getPackages(query: String): Result<List<NpmPackageInfo>> {
        return (searchPackages.filter {
                it.name.contains(query, ignoreCase = true) || it.description.contains(query, ignoreCase = true)
        } + packages.values.filter {
                it.name.contains(query, ignoreCase = true) || it.description.contains(query, ignoreCase = true)
        }).distinctBy { it.name }
            .also { coroutine.launch { searchPackages(query) } }
            .let { Result.success(it) }
    }

    override suspend fun searchPackages(query: String): Result<List<NpmPackageSearchResult>> {
        return npmDataSource.searchPackage(query)
            .onSuccess { searchPackages.addAll(it) }
    }

    // Version

    override fun latestVersion(packageName: String, version: String): Result<LatestVersion> {
        val currentVersion = version.cleanVersion() ?: return Result.failure(PackageNotFound())
        return getPackage(packageName).map {
            LatestVersion(version = it.version, isCurrent = currentVersion >= it.version)
        }
    }

    // Dependencies
    override suspend fun getDependencies(packageName: String, version: String): NpmDependency {
        return getSubDependencies(NpmDependency(packageName, version), 3)
    }

    private suspend fun getSubDependencies(dependency: NpmDependency, deepIndex: Int): NpmDependency {
        if (deepIndex < 0) return dependency
        val key = dependency.key
        return dependencies[key] ?: (npmDataSource.getDependencies(dependency)
            .let { list ->
                dependency.copy(
                    dependencies = list
                        .map { coroutine.async { getSubDependencies(it, deepIndex - 1) } }
                        .awaitAll()
                )
            }.also { dependencies[key] = it })
    }

    // Vulnerabilities
    override fun checkVulnerabilities(packageName: String, version: String): Result<NpmPackageVulnerabilities> {
        val key = "$packageName:$version"
        return vulnerabilities[key]
            ?.let { Result.success(it) }
            ?: run {
                coroutine.launch {
                    fetchPackage(packageName, version)
                        .flatMap {
//                            npmDataSource.checkDependenciesVulnerabilities(it)
                            npmDataSource.auditPackage(it)
                        }
                        .onSuccess { vulnerabilities[key] = it }
                }
                Result.failure(PackageNotFound())
            }
    }

    override suspend fun clear() {
        packages.clear()
        vulnerabilities.clear()
    }
}
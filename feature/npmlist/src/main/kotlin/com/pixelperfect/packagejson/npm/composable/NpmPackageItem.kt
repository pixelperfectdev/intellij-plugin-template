package com.pixelperfect.packagejson.npm.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.onClick
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import com.pixelperfect.packagejson.design.components.Icon
import com.pixelperfect.packagejson.design.components.TagText
import com.pixelperfect.packagejson.design.components.table.TableColumn
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.icons.Assets
import com.pixelperfect.packagejson.npm.presenter.DockerColumnType
import com.pixelperfect.packagejson.npm.presenter.NpmPackageViewState
import com.pixelperfect.packagejson.npm.presenter.PackageAction

@Composable
internal fun NpmPackageItem(
    viewState: NpmPackageViewState,
    onActionClick: (NpmPackageViewState, PackageAction) -> Unit
) {
    TableColumn(
        modifier = Modifier
            .defaultMinSize(minHeight = AppTheme.size.TableCollaboratorHeight)
            .fillMaxWidth(),
        columns = viewState.columns,
    ) { column ->
        when (column.columType) {
            DockerColumnType.All -> PackageColumn(
                packageViewState = viewState,
                onActionClick = onActionClick
            )
        }
    }
}

@Composable
private fun PackageColumn(
    packageViewState: NpmPackageViewState,
    onActionClick: (NpmPackageViewState, PackageAction) -> Unit
) {
    Column(
        modifier = Modifier,
        verticalArrangement = Arrangement.spacedBy(AppTheme.size.S)
    ) {
        Title(packageViewState, onActionClick)

        SelectionContainer {
            Text(
                text = packageViewState.description,
                style = AppTheme.typography.footnoteRegular,
                color = AppTheme.colors.text,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
        }

        Row(
            modifier = Modifier.horizontalScroll(rememberScrollState()),
            horizontalArrangement = Arrangement.spacedBy(AppTheme.size.M),
            verticalAlignment = Alignment.CenterVertically
        ) {
            packageViewState.tags.onEach { tag ->
                TagText(
                    text = tag,
                    borderColor = AppTheme.colors.textTertiary
                )
            }
        }
    }
}

@Composable
private fun Title(
    packageViewState: NpmPackageViewState,
    onActionClick: (NpmPackageViewState, PackageAction) -> Unit
) {
    Row(
        modifier = Modifier,
        horizontalArrangement = Arrangement.spacedBy(AppTheme.size.S),
        verticalAlignment = Alignment.CenterVertically
    ) {
        SelectionContainer {
            Text(
                text = packageViewState.name,
                style = AppTheme.typography.bodyMedium,
                color = AppTheme.colors.text,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
        }

        TagText(
            text = packageViewState.version,
            backgroundColor = AppTheme.colors.textTertiary
        )

        Text(
            text = packageViewState.date,
            style = AppTheme.typography.footnoteRegular,
            color = AppTheme.colors.text,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
        )

        Links(packageViewState, onActionClick)
    }
}

@Composable
private fun Links(packageViewState: NpmPackageViewState, onActionClick: (NpmPackageViewState, PackageAction) -> Unit) {
    val modifier = Modifier.size(AppTheme.size.L)
    Row(
        modifier = Modifier,
        horizontalArrangement = Arrangement.spacedBy(AppTheme.size.S),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            modifier = modifier.clickable { onActionClick(packageViewState, PackageAction.Copy) },
            imageVector = Assets.IcCopy,
            tint = AppTheme.colors.text
        )
        if (!packageViewState.homePageUrl.isNullOrEmpty()) {
            Icon(
                modifier = modifier.clickable { onActionClick(packageViewState, PackageAction.OpenHomePage) },
                imageVector = Assets.IcLink,
                tint = AppTheme.colors.text
            )
        }
        if (!packageViewState.npmUrl.isNullOrEmpty()) {
            Icon(
                modifier = modifier.clickable { onActionClick(packageViewState, PackageAction.OpenNpm) },
                imageVector = Assets.IcNpm,
                tint = AppTheme.colors.text
            )
        }
        if (!packageViewState.repositoryUrl.isNullOrEmpty()) {
            Icon(
                modifier = modifier.clickable { onActionClick(packageViewState, PackageAction.OpenRepository) },
                imageVector = Assets.IcGit,
                tint = AppTheme.colors.text
            )
        }
        if (!packageViewState.bugsUrl.isNullOrEmpty()) {
            Icon(
                modifier = modifier.clickable { onActionClick(packageViewState, PackageAction.OpenBug) },
                imageVector = Assets.IcBug,
                tint = AppTheme.colors.text
            )
        }
    }
}

@Composable
private fun CheckBoxColumn(
    checked: Boolean
) {
    Checkbox(
        checked = checked,
        onCheckedChange = {},
        colors = AppTheme.colors.checkBoxColors()
    )
}
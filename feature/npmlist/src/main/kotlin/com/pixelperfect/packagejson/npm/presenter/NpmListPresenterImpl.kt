package com.pixelperfect.packagejson.npm.presenter

import com.pixelperfect.packagejson.data.npm.NpmPackageSearchResult
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.ocpsoft.prettytime.PrettyTime
import java.time.ZoneId
import java.time.ZonedDateTime

class NpmListPresenterImpl: NpmListPresenter {

    private val _viewState = MutableStateFlow<NpmListViewState>(NpmListViewState.Idle)
    override val listViewState = _viewState.asStateFlow()

    private val _toolbarState = MutableStateFlow(NpmToolbarState())
    override val toolbarState = _toolbarState.asStateFlow()

    // Loading

    override fun displayIdle() {
        _viewState.value = NpmListViewState.Idle
    }

    override fun displayLoading(isLoading: Boolean) {
        _viewState.value = NpmListViewState.Loading
    }

    override fun displayPackageList(packages: List<NpmPackageSearchResult>) {
        if (packages.isEmpty()) {
            _viewState.value = NpmListViewState.EmptyList
        }
        else {
            _viewState.value = (listViewState.value as? NpmListViewState.Data ?: NpmListViewState.Data())
                .let { state -> state.copy(items = packages.map { it.toViewState() }) }
        }
    }

    override fun displaySearchQuery(query: String) {
        _toolbarState.value = NpmToolbarState(searchQuery = query)
    }

    // Other

    override fun clear() {
        _viewState.value = NpmListViewState.Idle
    }

    private fun NpmPackageSearchResult.toViewState() = NpmPackageViewState(
        name = this.name,
        version = this.version,
        description = this.description,
        tags = this.tags,
        dependencyCode = this.dependencyCode,
        date = this.date?.formatTimeAgo() ?: "",
        homePageUrl = this.homePageUrl,
        repositoryUrl = this.repositoryUrl,
        npmUrl = this.npmUrl,
        bugsUrl = this.bugsUrl,
    )

    private val prettyTime = PrettyTime();
    private fun String?.formatTimeAgo(): String {
        if (this.isNullOrBlank()) return ""
        return prettyTime.format(
            ZonedDateTime.parse(this)
                .withZoneSameInstant(ZoneId.systemDefault())
        )
    }
}
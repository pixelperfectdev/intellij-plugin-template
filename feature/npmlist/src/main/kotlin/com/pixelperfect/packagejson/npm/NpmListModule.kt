package com.pixelperfect.packagejson.npm

import com.pixelperfect.packagejson.common.AppInstance
import com.pixelperfect.packagejson.common.Presenter
import com.pixelperfect.packagejson.domain.usecase.npm.SearchNpmPackageUseCase
import com.pixelperfect.packagejson.npm.interactor.SearchNpmPackageInteractor
import com.pixelperfect.packagejson.npm.presenter.NpmListPresenter
import com.pixelperfect.packagejson.npm.presenter.NpmListPresenterImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val npmListModule = module {

    scope<AppInstance> {
        scoped {
            NpmListViewModel(
                presenter = get(),
                navigationManager = get(),
                searchNpmPackageUseCase = get(),
            )
        }

        scoped<NpmListPresenter> { NpmListPresenterImpl() } bind Presenter::class

        factory<SearchNpmPackageUseCase> {
            SearchNpmPackageInteractor(
                presenter = get(),
                npmRepository = get()
            )
        }
    }
}

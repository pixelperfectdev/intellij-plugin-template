package com.pixelperfect.packagejson.npm

import com.pixelperfect.packagejson.common.ViewModel
import com.pixelperfect.packagejson.domain.usecase.npm.SearchNpmPackageUseCase
import com.pixelperfect.packagejson.navigation.NavigationManager
import com.pixelperfect.packagejson.navigation.Route
import com.pixelperfect.packagejson.npm.presenter.NpmListPresenter
import com.pixelperfect.packagejson.npm.presenter.NpmPackageViewState
import com.pixelperfect.packagejson.npm.presenter.PackageAction
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class NpmListViewModel(
    private val presenter: NpmListPresenter,
    private val navigationManager: NavigationManager,
    private val searchNpmPackageUseCase: SearchNpmPackageUseCase,
): ViewModel() {

    val viewState = presenter.listViewState
    val toolbarState = presenter.toolbarState

    private var searchJob: Job? = null

    init {
        viewModelScope.launch {
        }
    }

    fun onSearchUpdate(query: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            searchNpmPackageUseCase(query)
        }
    }

    fun loadMore() {

    }

    fun onActionClick(packageViewState :NpmPackageViewState, action: PackageAction) {
        when (action) {
            PackageAction.Copy -> navigationManager.copyToClipboard(packageViewState.dependencyCode)
            PackageAction.OpenHomePage -> navigationManager.navigateToLink(packageViewState.homePageUrl!!)
            PackageAction.OpenRepository -> navigationManager.navigateToLink(packageViewState.repositoryUrl!!)
            PackageAction.OpenNpm -> navigationManager.navigateToLink(packageViewState.npmUrl!!)
            PackageAction.OpenBug -> navigationManager.navigateToLink(packageViewState.bugsUrl!!)
        }
    }

    fun onPackageClick(imageId: String) {
        navigationManager.navigateTo(Route.OpenDockerImage(imageId))
    }

    fun onUrlClick(url: String) {
        navigationManager.navigateToLink(url)
    }
}
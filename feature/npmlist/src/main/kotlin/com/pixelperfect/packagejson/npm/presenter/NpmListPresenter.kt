package com.pixelperfect.packagejson.npm.presenter

import com.pixelperfect.packagejson.common.Presenter
import com.pixelperfect.packagejson.data.npm.NpmPackageSearchResult
import kotlinx.coroutines.flow.StateFlow

interface NpmListPresenter: Presenter {

    val listViewState: StateFlow<NpmListViewState>
    val toolbarState: StateFlow<NpmToolbarState>

    // Loading
    fun displayIdle()

    fun displayLoading(isLoading: Boolean)
    fun displayPackageList(packages: List<NpmPackageSearchResult>)
    fun displaySearchQuery(query: String)

}

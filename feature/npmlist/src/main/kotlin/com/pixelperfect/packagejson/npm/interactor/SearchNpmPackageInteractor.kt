package com.pixelperfect.packagejson.npm.interactor

import com.pixelperfect.packagejson.domain.repository.NpmRepository
import com.pixelperfect.packagejson.domain.usecase.npm.SearchNpmPackageUseCase
import com.pixelperfect.packagejson.npm.presenter.NpmListPresenter

class SearchNpmPackageInteractor(
    private val presenter: NpmListPresenter,
    private val npmRepository: NpmRepository
): SearchNpmPackageUseCase {
    override suspend fun invoke(query: String) {
        presenter.displayLoading(true)
        presenter.displaySearchQuery(query)

        npmRepository.searchPackages(query = query)
            .onSuccess {
                presenter.displayLoading(false)
                presenter.displayPackageList(it)
            }
            .onFailure {
                presenter.displayLoading(false)
                presenter.displayPackageList(emptyList())
            }
    }
}
package com.pixelperfect.packagejson.npm.presenter

import com.pixelperfect.packagejson.design.viewstate.PageViewState
import com.pixelperfect.packagejson.design.viewstate.TableColumnType
import com.pixelperfect.packagejson.design.viewstate.TableColumnViewState
import com.pixelperfect.packagejson.design.viewstate.TableItemViewState
import com.pixelperfect.packagejson.design.viewstate.TableViewState

// Image list

sealed class NpmListViewState {
    object Idle: NpmListViewState()
    object Loading: NpmListViewState()
    object EmptyList: NpmListViewState()

    data class Data(
        override val items: List<NpmPackageViewState> = emptyList()
    ): NpmListViewState(), TableViewState<DockerColumnType, NpmPackageViewState> {
        override val columns: List<TableColumnViewState<DockerColumnType>> = dockerImageListColumns
        override val page: PageViewState = PageViewState.Disabled
    }
}

data class NpmPackageViewState(
    val name: String,
    val version: String,
    val description: String,
    val tags: List<String>,
    val dependencyCode: String,
    val date: String,
    val homePageUrl: String?,
    val repositoryUrl: String?,
    val npmUrl: String?,
    val bugsUrl: String?,
): TableItemViewState<DockerColumnType> {
    override val columns: List<TableColumnViewState<DockerColumnType>> = dockerImageListColumns
}

// Column
enum class DockerColumnType: TableColumnType { All }

private val dockerImageListColumns = listOf(
    TableColumnViewState(DockerColumnType.All, weight = 1f, title = "Name"),
)

// Toolbar
data class NpmToolbarState(
    val searchQuery: String = "",
)

enum class PackageAction {
    Copy,
    OpenHomePage,
    OpenRepository,
    OpenNpm,
    OpenBug,
}

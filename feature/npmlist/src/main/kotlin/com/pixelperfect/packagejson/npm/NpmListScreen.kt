package com.pixelperfect.packagejson.npm

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import com.pixelperfect.packagejson.design.components.EmptyView
import com.pixelperfect.packagejson.design.components.Loading
import com.pixelperfect.packagejson.design.components.table.Table
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.npm.composable.NpmPackageItem
import com.pixelperfect.packagejson.npm.presenter.NpmListViewState
import com.pixelperfect.packagejson.npm.presenter.NpmPackageViewState
import com.pixelperfect.packagejson.npm.presenter.PackageAction

@Composable
fun NpmListScreen(viewModel: NpmListViewModel) {
    val state = viewModel.viewState.collectAsState().value

    when (state) {
        NpmListViewState.Idle -> EmptyView(message = AppTheme.translation.common.search)
        NpmListViewState.Loading -> Loading(color = AppTheme.colors.text, size = AppTheme.size.XXL)
        NpmListViewState.EmptyList -> EmptyView(message = AppTheme.translation.common.search)
        is NpmListViewState.Data -> Table(
            modifier = Modifier,
            viewState = state,
            displayHeaders = false,
            onLoadMore = viewModel::loadMore,
            onItemClick = {  },
        ) { _, item, _ ->
            NpmPackageItem(item, viewModel::onActionClick)
        }
    }
}
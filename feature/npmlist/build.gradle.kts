plugins {
    id("multiplatform-setup")
    id("multiplatform-compose-setup")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":data"))
    implementation(project(":navigation"))
    implementation(project(":domain"))
    implementation(project(":design-system"))
    implementation(project(":translation"))
    implementation("org.ocpsoft.prettytime:prettytime:5.0.7.Final")
}
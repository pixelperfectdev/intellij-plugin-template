plugins {
    id("multiplatform-setup")
    id("org.jetbrains.compose")
}

dependencies {
    compileOnly(compose.ui)
    compileOnly(compose.material)
    compileOnly(compose.runtime)
    compileOnly(compose.foundation)
    compileOnly(compose.desktop.common)
    implementation(project(":translation"))
}
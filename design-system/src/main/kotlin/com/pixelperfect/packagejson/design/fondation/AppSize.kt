package com.pixelperfect.packagejson.design.fondation

import androidx.compose.runtime.Immutable
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Immutable
data class AppSize internal constructor(
    val None: Dp = 0.dp,
    val XXXS: Dp = 1.dp,
    val XXS: Dp = 2.dp,
    val XS: Dp = 4.dp,
    val SS: Dp = 6.dp,
    val S: Dp = 8.dp,
    val M: Dp = 12.dp,
    val L: Dp = 16.dp,
    val XL: Dp = 24.dp,
    val XXL: Dp = 32.dp,
    val XXXL: Dp = 48.dp,
    val XXXXL: Dp = 64.dp,

    // Design System
    val ButtonCtaHeight: Dp = 42.dp,
    val TextInputHeight: Dp = 42.dp,
    val SwitchHeight: Dp = 26.dp,
    // Signin
    val SigninScreenLogo: Dp = 94.dp,
    // Table
    val TableSiteHeight: Dp = 94.dp,
    val TableBuildHeight: Dp = 94.dp,
    val TableVariableHeight: Dp = 64.dp,
    val TableCollaboratorHeight: Dp = 64.dp,
    // Toolbar
    val ToolbarHeight: Dp = 38.dp,
    // Build
    val StageStatusIcon: Dp = 30.dp,
    val BuildInfoIcon: Dp = 14.dp,
    // Height
    val HeaderHeight: Dp = 62.dp,
    val HeaderLogo: Dp = 32.dp,
    val StatusLabelHeight: Dp = 24.dp,

)

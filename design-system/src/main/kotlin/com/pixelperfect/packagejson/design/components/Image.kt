package com.pixelperfect.packagejson.design.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.produceState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.loadImageBitmap
import androidx.compose.ui.res.loadSvgPainter
import androidx.compose.ui.res.loadXmlImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Density
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.xml.sax.InputSource
import java.io.File
import java.net.URL

// https://github.com/JetBrains/compose-jb/blob/master/tutorials/Image_And_Icons_Manipulations/README.md
@Composable
fun Image(
    modifier: Modifier = Modifier,
    url: String?,
    contentDescription: String = "",
    contentScale: ContentScale = ContentScale.FillWidth,
    fallback: String? = null,
    fallbackContent: (@Composable () -> Unit)? = null,
) {
    if (url?.contains("""(?:(http|https)(://))""".toRegex()) == true) {
        AsyncImage(
            modifier = modifier,
            url = url,
            fallback = fallback,
            fallbackContent = fallbackContent,
            load = { loadImageBitmap(it) },
            painterFor = { remember { BitmapPainter(it) } },
            contentDescription = contentDescription,
            contentScale = contentScale
        )
    }
    else fallbackContent?.invoke()
}

@Composable
private fun <T> AsyncImage(
    modifier: Modifier,
    load: suspend (String) -> T,
    url: String,
    fallback: String?,
    fallbackContent: (@Composable () -> Unit)?,
    painterFor: @Composable (T) -> Painter,
    contentDescription: String,
    contentScale: ContentScale = ContentScale.Fit,
) {
    val image: T? by produceState<T?>(null, url) {
        value = withContext(Dispatchers.IO) {
            try {
                load(url)
            } catch (e: Exception) {
                null
            }
        }
    }

    if (image != null) {
        androidx.compose.foundation.Image(
            modifier = modifier,
            painter = painterFor(image!!),
            contentDescription = contentDescription,
            contentScale = contentScale,
        )
    }
    else if (fallback != null) {
        androidx.compose.foundation.Image(
            modifier = modifier,
            painter = painterResource(fallback),
            contentDescription = contentDescription,
            contentScale = contentScale,
        )
    }
    else fallbackContent?.invoke()
}

/* Loading from file with java.io API */

private fun loadImageBitmap(file: File): ImageBitmap =
    file.inputStream().buffered().use(::loadImageBitmap)

private fun loadSvgPainter(file: File, density: Density): Painter =
    file.inputStream().buffered().use { loadSvgPainter(it, density) }

private fun loadXmlImageVector(file: File, density: Density): ImageVector =
    file.inputStream().buffered().use { loadXmlImageVector(InputSource(it), density) }

/* Loading from network with java.net API */

private fun loadImageBitmap(url: String): ImageBitmap =
    URL(url).openStream().buffered().use(::loadImageBitmap)

private fun loadSvgPainter(url: String, density: Density): Painter =
    URL(url).openStream().buffered().use { loadSvgPainter(it, density) }

private fun loadXmlImageVector(url: String, density: Density): ImageVector =
    URL(url).openStream().buffered().use { loadXmlImageVector(InputSource(it), density) }

package com.pixelperfect.packagejson.design.viewstate

sealed class NotificationViewState {

    object None: NotificationViewState()

    sealed class Dialog: NotificationViewState() {
        data class FilePicker(val onFilePicked: (String?) -> Unit): Dialog()
        object FolderPicker: Dialog()
    }

    sealed class Banner(val title: String, val message: String, val type: BannerState) : NotificationViewState() {
        class Default(title: String, message: String) : Banner(title, message, BannerState.Success)
        class Error(title: String, message: String) : Banner(title, message, BannerState.Success)
    }
}
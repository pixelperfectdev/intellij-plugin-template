package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun SwitchSelector(
    modifier: Modifier = Modifier,
    enabled: Boolean,
    isChecked: Boolean,
    onCheckUpdate: (Boolean) -> Unit,
    checkedLabel: String,
    uncheckedLabel: String,
    colorSelected: Color = AppTheme.colors.primary,
    colorTextChecked: Color = AppTheme.colors.text,
    colorTextUnchecked: Color = AppTheme.colors.textSecondary,
) {
    Row(
        modifier = modifier
            .height(AppTheme.size.SwitchHeight)
            .border(
                width = AppTheme.size.XXXS,
                color = AppTheme.colors.switchBorder,
                shape = MaterialTheme.shapes.small
            )
            .clip(shape = MaterialTheme.shapes.small),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .background(
                    color = if (isChecked) colorSelected else AppTheme.colors.transparent,
                    shape = MaterialTheme.shapes.small
                )
                .clickable(enabled = enabled) { onCheckUpdate(true) }
                .padding(horizontal = AppTheme.size.S),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = AnnotatedString(checkedLabel.uppercase()),
                style = AppTheme.typography.footnoteMedium,
                color = if (isChecked) colorTextChecked else colorTextUnchecked,
                maxLines = 1
            )
        }

        Box(
            modifier = Modifier
                .fillMaxHeight()
                .background(
                    color = if (!isChecked) colorSelected else AppTheme.colors.transparent,
                    shape = MaterialTheme.shapes.small
                )
                .clickable(enabled = enabled) { onCheckUpdate(false) }
                .padding(horizontal = AppTheme.size.S),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = uncheckedLabel.uppercase(),
                style = AppTheme.typography.footnoteMedium,
                color = if (!isChecked) colorTextChecked else colorTextUnchecked,
                maxLines = 1
            )
        }
    }
}
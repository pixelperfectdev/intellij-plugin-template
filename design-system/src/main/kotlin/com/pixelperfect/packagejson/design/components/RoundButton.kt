package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun RoundButton(
    modifier: Modifier = Modifier,
    text: String,
    isEnabled: Boolean = true,
    onClick: () -> Unit,
    isLoading: Boolean = false,
    loaderColor: Color = AppTheme.colors.background,
) {
    Row(
        modifier = modifier
            .clickable(enabled = isEnabled) { onClick() }
            .background(
                color = if (isEnabled) AppTheme.colors.text else AppTheme.colors.textTertiary,
                shape = RoundedCornerShape(AppTheme.size.XXL)
            )
            .requiredHeight(IntrinsicSize.Min)
            .padding(horizontal = AppTheme.size.L, vertical = AppTheme.size.SS),
        horizontalArrangement = Arrangement.spacedBy(AppTheme.size.S),
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (isLoading) {
            Loading(
                color = loaderColor,
                strokeSize = AppTheme.size.XXS,
                size = AppTheme.size.L
            )
        }

        Text(
            text = text,
            style = AppTheme.typography.footnoteMedium,
            color = AppTheme.colors.background,
        )
    }
}

package com.pixelperfect.packagejson.design.fondation

import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

open class AppColorPalette(isDarkMode: Boolean = false) {
    // Android
    val primary = if (isDarkMode) Blue400 else Blue500
    open val primaryVariant = if (isDarkMode) Blue500 else Blue400
    open val background = if (isDarkMode) DarkBackground else LightBackground
    open val success = Green
    open val warning = Orange
    open val yellow = Yellow

    // Status
    open val completed = Green
    open val error = Red
    open val building = Blue400
    open val canceled = Black30
    open val pending = Yellow
    open val unknown = Orange

    // Text
    open val disabled = Gray500
    open val text = if (isDarkMode) White else Black
    open val textSecondary = if (isDarkMode) Black else White
    open val textTertiary = if (isDarkMode) Gray800 else Gray300

    // Divider
    open val divider = (if (isDarkMode) White else Black).copy(alpha = 0.20f)

    // Tooltip
    open val tooltipBg = if (isDarkMode) Black80 else White80
    open val tooltipText = if (isDarkMode) White else Black

    // Button
    open val textButton = if (isDarkMode) Black else White
    open val button = primary

    // Checkbox
    open val checked = if (isDarkMode) LightBlue else DarkBlue
    open val unchecked = if (isDarkMode) LightBlue else DarkBlue
    open val checkmark = if (isDarkMode) Black else White

    // Transparent
    open val transparent = Transparent

    // Sidebar
    open val sidebarBg = if (isDarkMode) LightBlue else DarkBlue
    open val sidebarSearchBarBg = if (isDarkMode) White30 else Black30
    open val sidebarSearchBarBorder = if (isDarkMode) Black20 else White20
    open val sidebarSearchBarText = if (isDarkMode) Black else White
    open val sidebarTextTitle = if (isDarkMode) Black else White
    open val sidebarText = if (isDarkMode) Black else White
    open val sidebarTextSubtitle = Gray500
    open val sidebarSplitter = if (isDarkMode) Black else White
    open val sidebarLoader = if (isDarkMode) Black else White

    // Tab
    open val tabsBg = Gray200
    open val tabItemNoSelected = Gray100
    open val tabItemSelected = if (isDarkMode) DarkBackground else LightBackground
    open val tabText = if (isDarkMode) White else Black
    open val tabClose = if (isDarkMode) White else Black

    // BottomBar
    open val bottomBarBorder = if (isDarkMode) Black20 else White20

    // Switch
    open val switchBorder = if (isDarkMode) White20 else Black20

    // Banner
    open val bannerText = if (isDarkMode) White else Black
    open val bannerBgDefault = if (isDarkMode) Black else White
    open val bannerBgSuccess = if (isDarkMode) Black else White
    open val bannerBgWarning = if (isDarkMode) Black else White
    open val bannerBgError = Red

    // Signin
    open val signinLogo = Color.Unspecified

    // Platform
    open val platformAndroid = Green
    open val platformIos = Black50
    open val platformMacos = Black70
    open val platformCordova = Gray800
    open val platformIonic = Gray500
    open val platformFlutter = Blue400
    open val platformReactNative = Blue600
    open val platformUnknown = Gray500

    // Header
    open val headerBackground = DarkGrey
    open val headerIcon = Gray200

    // Button
    @Composable
    fun buttonCta() = ButtonDefaults.textButtonColors(
        backgroundColor = primary,
        contentColor = textButton,
        disabledContentColor = disabled
    )

    fun lightColors() = Colors(
        primary = text,
        primaryVariant = text,
        secondary = text,
        secondaryVariant = text,
        background = background,
        surface = text,
        error = error,
        onPrimary = text,
        onSecondary = text,
        onBackground = text,
        onSurface = text,
        onError = text,
        isLight = true
    )

    @Composable
    open fun checkBoxColors() = CheckboxDefaults.colors(
        checkedColor = primary,
        uncheckedColor = primary,
        checkmarkColor = primary,
        disabledColor = primary,
        disabledIndeterminateColor = primary,
    )

    companion object {
        val Black = Color(0xFF000000)
        val Black90 = Color(0xE6000000)
        val Black80 = Color(0xCC000000)
        val Black70 = Color(0xB3000000)
        val Black60 = Color(0x99000000)
        val Black50 = Color(0x80000000)
        val Black40 = Color(0x66000000)
        val Black30 = Color(0x4D000000)
        val Black20 = Color(0x33000000)
        val Black10 = Color(0x1A000000)

        val White = Color(0xFFFFFFFF)
        val White90 = Color(0xE6FFFFFF)
        val White80 = Color(0xCCFFFFFF)
        val White70 = Color(0xB3FFFFFF)
        val White60 = Color(0x99FFFFFF)
        val White50 = Color(0x80FFFFFF)
        val White40 = Color(0x66FFFFFF)
        val White30 = Color(0x4DFFFFFF)
        val White20 = Color(0x33FFFFFF)
        val White10 = Color(0x1AFFFFFF)

        val Gray100 = Color(0xFFEFEFEF)
        val Gray200 = Color(0xFFE8E8E8)
        val Gray300 = Color(0xFFDFDFDF)
        val Gray400 = Color(0xFFCFCFCF)
        val Gray500 = Color(0xFFBFBFBF)
        val Gray800 = Color(0xFF7F7F7F)

        val Blue200 = Color(0xFFE3E9FF)
        val Blue300 = Color(0xFF8AA0F1)
        val Blue400 = Color(0xFF4D71F3)
        val Blue500 = Color(0xFF2250F4)
        val Blue600 = Color(0xFF0028B5)

        val DarkGrey = Color(0xFF141414)

        val LightBackground = Color(0xFFEBEBEB)
        val DarkBackground = Color(0xFF333333)

        val LightBlue = Color(0xFF3D65F5)
        val DarkBlue = Color(0xFF020A27)

        val Red = Color(0xFFDF2935)
        val Green = Color(0xFF29E0B9)
        val Orange = Color(0xFFF0A202)
        val Yellow = Color(0xFFE8D33F)

        val Transparent = Color(0x00000000)
    }
}


package com.pixelperfect.packagejson.design.fondation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

interface MarkdownRenderer {
    @Composable
    fun Markdown(
        modifier: Modifier,
        content: String
    )

    @Composable
    fun WebView(
        modifier: Modifier,
        content: String,
        onBrowserSetup: (Any) -> Unit
    )
}
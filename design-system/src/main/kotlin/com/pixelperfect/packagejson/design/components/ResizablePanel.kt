package com.pixelperfect.packagejson.design.components

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.SpringSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp

@Composable
fun ResizablePanel(
    modifier: Modifier,
    state: PanelState,
    content: @Composable () -> Unit,
) {
    val alpha by animateFloatAsState(if (state.isExpanded) 1f else 0f, SpringSpec(stiffness = Spring.StiffnessLow))

    Box(modifier) {
        Box(modifier = Modifier.fillMaxSize().graphicsLayer(alpha = alpha)) {
            content()
        }

//        Icon(
//            if (state.isExpanded) Icons.Default.ArrowBack else Icons.Default.ArrowForward,
//            contentDescription = if (state.isExpanded) "Collapse" else "Expand",
//            tint = AppTheme.colors.onBackground,
//            modifier = Modifier
//                .padding(top = 4.dp)
//                .width(24.dp)
//                .clickable { state.isExpanded = !state.isExpanded }
//                .padding(4.dp)
//                .align(Alignment.TopEnd)
//        )
    }
}

class PanelState {
    val collapsedSize = 24.dp
    var expandedSize by mutableStateOf(300.dp)
    val expandedSizeMin = 200.dp
    var isExpanded by mutableStateOf(true)
    val splitter = SplitterState()
}

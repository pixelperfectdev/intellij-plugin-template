package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.TooltipArea
import androidx.compose.foundation.TooltipPlacement
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Tooltip(
    modifier: Modifier = Modifier,
    title: String,
    content: @Composable () -> Unit
) {
    TooltipArea(
        modifier = modifier,
        delayMillis = AppTheme.duration.TooltipDelay,
        tooltipPlacement = TooltipPlacement.CursorPoint(
            offset = DpOffset(x = 2.dp, y = 2.dp),
            alignment = Alignment.BottomEnd
        ),
        tooltip = {
            if (title.isNotEmpty()) {
                Text(
                    modifier = Modifier
                        .background(AppTheme.colors.tooltipBg)
                        .padding(horizontal = AppTheme.size.SS, vertical = AppTheme.size.XS),
                    text = title,
                    style = AppTheme.typography.footnoteRegular,
                    color = AppTheme.colors.tooltipText
                )
            }
        },
        content = content
    )
}
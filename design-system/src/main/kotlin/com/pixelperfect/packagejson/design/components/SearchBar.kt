package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun SearchBar(
    modifier: Modifier = Modifier,
    value: String,
    hint: String = "Search",
    onValueChanged: (String) -> Unit,
    textColor: Color = AppTheme.colors.textSecondary,
    hintColor: Color = AppTheme.colors.textTertiary,
) {
    val interactionSource = remember { MutableInteractionSource() }
    BasicTextField(
        modifier = modifier
            .height(AppTheme.size.TextInputHeight)
            .border(
                width = AppTheme.size.XXXS,
                color = AppTheme.colors.sidebarSearchBarBorder,
                shape = MaterialTheme.shapes.small
            )
            .padding(
                vertical = AppTheme.size.XS,
                horizontal = AppTheme.size.M
            ),
        value = value,
        textStyle = AppTheme.typography.subtitle.copy(color = textColor),
        onValueChange = onValueChanged,
        interactionSource = interactionSource,
        singleLine = true,
        cursorBrush = SolidColor(textColor),
    ) { innerTextField ->
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                modifier = Modifier.weight(1f)
            ) {
                if (value.isEmpty()) {
                    Text(
                        text = hint,
                        color = hintColor,
                        style = AppTheme.typography.bodyRegular,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1
                    )
                }
                innerTextField()
            }

            Icon(
                modifier = Modifier
                    .size(32.dp)
                    .padding(4.dp)
                    .clickable { onValueChanged("") },
                imageVector = Icons.Default.Clear,
                tint = AppTheme.colors.sidebarSearchBarText,
                contentDescription = "Clear"
            )
        }
    }
}


package com.pixelperfect.packagejson.design.components.table

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.pixelperfect.packagejson.design.components.VerticalDivider
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.viewstate.TableColumnType
import com.pixelperfect.packagejson.design.viewstate.TableColumnViewState

@Composable
fun<T, U> TableColumn(
    modifier: Modifier = Modifier,
    columns: List<T>,
    horizontalPadding: Dp =  AppTheme.size.M,
    verticalPadding: Dp =  AppTheme.size.S,
    alignment: Alignment = Alignment.CenterStart,
    divider: @Composable () -> Unit = { VerticalDivider() },
    content: @Composable (T) -> Unit,
) where U: TableColumnType, T: TableColumnViewState<U> {
    Row(
        modifier = modifier.height(IntrinsicSize.Min),
    ) {
        columns.mapIndexed { index, item ->
            Box(
                modifier = Modifier
                    .let {
                        if (item.width < 0) it.weight(item.weight)
                        else it.requiredWidth(item.width.dp)
                    },
                contentAlignment = Alignment.CenterEnd
            ) {
                Box(
                    modifier = modifier
                        .fillMaxSize()
                        .padding(horizontal = horizontalPadding, vertical = verticalPadding),
                    contentAlignment = alignment
                ) {
                    content(item)
                }

                if (index < columns.lastIndex)
                    divider()
            }
        }
    }
}
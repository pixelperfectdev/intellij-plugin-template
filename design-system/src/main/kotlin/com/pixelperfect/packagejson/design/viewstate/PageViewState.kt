package com.pixelperfect.packagejson.design.viewstate

data class PageViewState(
    val isLoadingMore: Boolean,
    val isPagingEnabled: Boolean,
) {
    companion object {
        val Disabled = PageViewState(
            isLoadingMore = false,
            isPagingEnabled = false,
        )
        val Default = PageViewState(
            isLoadingMore = false,
            isPagingEnabled = true
        )
    }
}

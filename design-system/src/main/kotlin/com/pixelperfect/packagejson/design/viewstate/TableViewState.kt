package com.pixelperfect.packagejson.design.viewstate

interface TableViewState<T: TableColumnType, U: TableItemViewState<T>> {
    val items: List<U>
    val columns: List<TableColumnViewState<T>>
    val page: PageViewState
}

data class TableColumnViewState<T: TableColumnType>(
    val columType: T,
    val title: String,
    val weight: Float = 1f,
    val width: Int = -1,
    val isAscendant: Boolean = true,
    val helper: String? = null
)

interface TableItemViewState<T: TableColumnType> {
    val columns: List<TableColumnViewState<T>>
}

interface TableColumnType
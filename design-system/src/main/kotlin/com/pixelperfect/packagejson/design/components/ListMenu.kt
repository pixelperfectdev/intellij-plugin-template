package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun ListMenu(
    modifier: Modifier = Modifier,
    value: String,
    values: List<String>,
    onValueSelected: (String) -> Unit,
    editable: Boolean = false,
    hint: String = "",
    enabled: Boolean = true,
    height: Dp = AppTheme.size.TextInputHeight,
    minHeight: Dp = height,
    textStyle: TextStyle = AppTheme.typography.bodyRegular,
    textColor: Color = AppTheme.colors.textSecondary,
    hintColor: Color = AppTheme.colors.textTertiary,
    shape: Shape = MaterialTheme.shapes.small,

    ) {
    var expanded by remember { mutableStateOf(false) }
    val icon = if (expanded) Icons.Filled.KeyboardArrowUp else Icons.Filled.KeyboardArrowDown

    TextInput(
        modifier = modifier,
        value = value,
        onValueChanged = { if (editable) onValueSelected(it)  },
        enabled = true, // to detect focus
        hint = hint,
        height = height,
        minHeight = minHeight,
        textStyle = textStyle,
        textColor = textColor,
        hintColor = hintColor,
        onFocused = { expanded = true },
        shape = shape,
        trailingIcon = {
            Box {
                Icon(
                    modifier = Modifier
                        .size(AppTheme.size.XL)
                        .clickable { expanded = !expanded },
                    contentDescription = null,
                    imageVector = icon,
                    tint = AppTheme.colors.text,
                )
                DropdownMenu(
                    modifier = Modifier.background(AppTheme.colors.background),
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                ) {
                    values.forEach { label ->
                        DropdownMenuItem(onClick = {
                            expanded = false
                            onValueSelected(label)
                        }) {
                            Text(
                                text = label,
                                style = AppTheme.typography.bodyMedium,
                                color = textColor
                            )
                        }
                    }
                }
            }
        }
    )
}

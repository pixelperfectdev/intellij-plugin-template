package com.pixelperfect.packagejson.design.fondation

import androidx.compose.runtime.Immutable
import kotlin.time.Duration.Companion.seconds

@Immutable
data class AppDuration
internal constructor(
    val ExpandModalAnimation: Int = 500,
    val ExpandBannerAnimation: Int = 500,
    val TooltipDelay: Int = 0.6.seconds.inWholeMilliseconds.toInt()
)
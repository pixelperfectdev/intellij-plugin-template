package com.pixelperfect.packagejson.design.fondation

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.runtime.*
import com.pixelperfect.packagejson.translation.Translation

object AppTheme {
    val colors: AppColorPalette
        @Composable
        @ReadOnlyComposable
        get() = LocalColors.current

    val typography: AppTypography
        @Composable
        @ReadOnlyComposable
        get() = LocalTypography.current

    val size: AppSize
        @Composable
        @ReadOnlyComposable
        get() = LocalSize.current

    val duration: AppDuration
        @Composable
        @ReadOnlyComposable
        get() = LocalDuration.current

    val translation: Translation
        @Composable
        @ReadOnlyComposable
        get() = LocalTranslation.current
}

@Composable
fun AppTheme(
    colors: AppColorPalette = AppColorPalette(),
    typography: AppTypography = AppTypography(),
    size: AppSize = AppSize(),
    translation: Translation = Translation(),
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(
        LocalColors provides colors,
        LocalTypography provides typography,
        LocalSize provides size,
        LocalTranslation provides translation,
    ) {
        MaterialTheme(
            colors = colors.lightColors(),
            typography = typography.materialTypography,
            shapes = Shapes(),
            content = content
        )
    }
}

internal val LocalColors = staticCompositionLocalOf { AppColorPalette() }
internal val LocalTypography = staticCompositionLocalOf { AppTypography() }
internal val LocalSize = staticCompositionLocalOf { AppSize() }
internal val LocalDuration = staticCompositionLocalOf { AppDuration() }
internal val LocalTranslation = staticCompositionLocalOf { Translation() }

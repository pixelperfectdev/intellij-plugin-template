package com.pixelperfect.packagejson.design.icons

fun Assets.toPath(): String {
    return "/icons/${this.name.toSnakeCase()}.svg"
}

private fun String.toSnakeCase() = fold(StringBuilder(length)) { acc, c ->
    if (c in 'A'..'Z') (if (acc.isNotEmpty()) acc.append('_') else acc).append(c + ('a' - 'A'))
    else acc.append(c)
}.toString()
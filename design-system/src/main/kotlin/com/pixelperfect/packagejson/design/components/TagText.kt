package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun TagText(
    text: String,
    borderColor: Color = Color.Unspecified,
    backgroundColor: Color = Color.Unspecified
) {
    Box(
        modifier = Modifier
            .background(
                color = backgroundColor,
                shape = RoundedCornerShape(AppTheme.size.XXL)
            )
            .border(width = AppTheme.size.XXXS, color = borderColor, shape = RoundedCornerShape(AppTheme.size.XXL))
            .padding(vertical = AppTheme.size.XS, horizontal = AppTheme.size.S)
    ) {
        Text(
            text = text,
            style = AppTheme.typography.captionMedium,
            color = AppTheme.colors.text,
            maxLines = 1
        )
    }
}
package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun TextInput(
    modifier: Modifier = Modifier,
    value: String,
    hint: String = "",
    onValueChanged: (String) -> Unit,
    onFocused: () -> Unit = {},
    height: Dp = AppTheme.size.TextInputHeight,
    minHeight: Dp = height,
    textStyle: TextStyle = AppTheme.typography.bodyRegular,
    textColor: Color = AppTheme.colors.textSecondary,
    hintColor: Color = AppTheme.colors.textTertiary,
    trailingIcon: @Composable (() -> Unit)? = null,
    enabled: Boolean = true,
    border: Boolean = true,
    singleLine: Boolean = true,
    shape: Shape = MaterialTheme.shapes.small
) {
    val hasFocus = remember { mutableStateOf(false) }
    if (hasFocus.value) onFocused()
    val borderWidth = if (hasFocus.value) AppTheme.size.XXS else  AppTheme.size.XXXS

    val interactionSource = remember { MutableInteractionSource() }
    BasicTextField(
        modifier = modifier
            .defaultMinSize(minHeight = minHeight)
            .height(height)
            .background(AppTheme.colors.transparent)
            .let {
                if (border) it.border(width = borderWidth, color = textColor, shape = shape)
                else it
            }
            .onFocusChanged { focusState -> hasFocus.value = focusState.isFocused || focusState.hasFocus }
            .padding(vertical = AppTheme.size.XS, horizontal = AppTheme.size.M),
        value = value,
        textStyle = textStyle.copy(color = textColor),
        onValueChange = onValueChanged,
        interactionSource = interactionSource,
        singleLine = singleLine,
        maxLines = 5,
        cursorBrush = SolidColor(textColor),
        enabled = enabled
    ) { innerTextField ->
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                modifier = Modifier.weight(1f)
            ) {
                if (value.isEmpty()) {
                    Text(
                        text = hint,
                        color = hintColor,
                        style = textStyle,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1
                    )
                }
                innerTextField()
            }

            trailingIcon?.invoke()
        }
    }
}
package com.pixelperfect.packagejson.design.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.viewstate.BannerState
import java.util.*

@Composable
fun AnimateBanner(
    bannerState: BannerState,
    message: String
) {
    var isBannerVisible by remember(bannerState) { mutableStateOf(false) }

    LaunchedEffect(bannerState) {
        Timer().scheduleAtFixedRate(object: TimerTask() {
            override fun run() {
                isBannerVisible = false
                cancel()
            } }, 5000, 5000
        )
        isBannerVisible = true
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        AnimatedVisibility(
            visible = isBannerVisible,
            enter = slideInVertically(),
            exit = slideOutVertically(
                targetOffsetY = { fullHeight -> -fullHeight },
                animationSpec = tween(durationMillis = AppTheme.duration.ExpandBannerAnimation)
            )
        ) {
            Banner(
                modifier = Modifier.padding(horizontal = AppTheme.size.L, vertical = 40.dp),
                bannerState = bannerState,
                text = message
            )
        }
    }
}

@Composable
fun Banner(
    modifier: Modifier = Modifier,
    text: String,
    bannerState: BannerState
) {
    val interactionSource: MutableInteractionSource = remember { MutableInteractionSource() }

    val textColor = AppTheme.colors.bannerText
    val bgColor = when (bannerState) {
        is BannerState.Success -> AppTheme.colors.bannerBgSuccess
        is BannerState.Warning -> AppTheme.colors.bannerBgWarning
        is BannerState.Error -> AppTheme.colors.bannerBgError
    }

    Surface(
        shape = MaterialTheme.shapes.small,
        color = bgColor,
        contentColor = textColor,
        modifier = modifier
            .clip(MaterialTheme.shapes.small)
            .indication(interactionSource, rememberRipple())
    ) {
        Row(
            modifier = Modifier.padding(AppTheme.size.L),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = text,
                style = AppTheme.typography.bodyMedium,
                color = textColor
            )
        }
    }
}

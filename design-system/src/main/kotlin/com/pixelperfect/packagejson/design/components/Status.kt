package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.pixelperfect.packagejson.design.fondation.AppColorPalette
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.viewstate.StatusTypeViewState
import com.pixelperfect.packagejson.design.viewstate.StatusViewState

@Composable
fun Status(
    modifier: Modifier = Modifier,
    status: StatusViewState,
    onClick: () -> Unit
) {
    Box(
        modifier = modifier
            .height(AppTheme.size.StatusLabelHeight)
            .clickable { onClick() }
            .background(
                color = when (status.type) {
                    StatusTypeViewState.Completed -> AppTheme.colors.completed
                    StatusTypeViewState.Error -> AppTheme.colors.error
                    StatusTypeViewState.Building -> AppTheme.colors.building
                    StatusTypeViewState.Canceled -> AppTheme.colors.canceled
                    StatusTypeViewState.Pending -> AppTheme.colors.pending
                    StatusTypeViewState.Skipped -> AppTheme.colors.unknown
                    StatusTypeViewState.Manual -> AppTheme.colors.unknown
                    StatusTypeViewState.Unknown -> AppTheme.colors.unknown
                },
                shape = RoundedCornerShape(AppTheme.size.XXL)
            )
            .padding(horizontal = AppTheme.size.M),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = status.tag,
            style = AppTheme.typography.captionMedium,
            color = AppTheme.colors.textSecondary,
            maxLines = 1
        )
    }
}

@Composable
fun LabelStatus(
    title: String,
    onClick: (() -> Unit)? = null,
    color: Color = when ((title.hashCode() + title.length).mod(9)) {
        0 -> AppColorPalette.Blue300
        1 -> AppColorPalette.Blue400
        2 -> AppColorPalette.Blue500
        3 -> AppColorPalette.LightBlue
        4 -> AppColorPalette.Black10
        5 -> AppColorPalette.Black40
        6 -> AppColorPalette.Black80
        7 -> AppColorPalette.DarkBlue
        else -> AppColorPalette.Gray800
    }
) {
    Box(
        modifier = Modifier
            .height(AppTheme.size.StatusLabelHeight)
            .clickable(enabled = onClick != null) { onClick?.invoke() }
            .background(
                color = color,
                shape = RoundedCornerShape(AppTheme.size.XXL)
            )
            .padding(horizontal = AppTheme.size.M),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = title,
            style = AppTheme.typography.captionMedium,
            color = AppTheme.colors.textSecondary,
            maxLines = 1
        )
    }
}
package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun VerticalDivider(
    modifier: Modifier = Modifier
        .fillMaxHeight(),
    color: Color = AppTheme.colors.divider,
    thickness: Dp = AppTheme.size.XXS
) {
    Spacer(
        modifier = modifier
            .width(thickness)
            .background(color),
    )
}

@Composable
fun HorizontalDivider(
    modifier: Modifier = Modifier,
    color: Color = AppTheme.colors.divider,
    thickness: Dp = AppTheme.size.XXS
) {
    Divider(
        modifier = modifier,
        color = color,
        thickness = thickness
    )
}
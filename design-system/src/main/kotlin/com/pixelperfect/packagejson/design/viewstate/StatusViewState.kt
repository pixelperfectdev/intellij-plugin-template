package com.pixelperfect.packagejson.design.viewstate

data class StatusViewState(
    val tag: String,
    val type: StatusTypeViewState
)

enum class StatusTypeViewState {
    Completed,
    Error,
    Building,
    Canceled,
    Pending,
    Skipped,
    Manual,
    Unknown,
}

package com.pixelperfect.packagejson.design.viewstate

interface LoadingViewState
interface DataViewState
interface ErrorViewState {
    val message: String
}

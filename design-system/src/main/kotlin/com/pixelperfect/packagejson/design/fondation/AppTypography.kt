package com.pixelperfect.packagejson.design.fondation

import androidx.compose.material.Typography
import androidx.compose.runtime.Immutable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.platform.Font
import androidx.compose.ui.unit.sp

fun robotoFontFamily() = FontFamily(
    Font("/font/roboto_bold.ttf", FontWeight.Bold, FontStyle.Normal),
    Font("/font/roboto_medium.ttf", FontWeight.Medium, FontStyle.Normal),
    Font("/font/roboto_regular.ttf", FontWeight.Normal, FontStyle.Normal)
)

@Immutable
open class AppTypography(
    fontFamily: FontFamily = robotoFontFamily(),
    size: Int = 15
) {
    private val fontSize = size / 15.0

    // Headline
    val headline: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 28).sp,
        lineHeight = (fontSize * 36).sp,
        fontWeight = FontWeight.Bold
    )
    // Title
    val title: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 22).sp,
        lineHeight = (fontSize * 28).sp,
        fontWeight = FontWeight.Bold
    )
    // Subtitle
    val subtitle: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 18).sp,
        lineHeight = (fontSize * 22).sp,
        fontWeight = FontWeight.Bold
    )
    val subtitleMedium: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 18).sp,
        lineHeight = (fontSize * 22).sp,
        fontWeight = FontWeight.Medium
    )
    // Body
    val bodyBold: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 15).sp,
        lineHeight = (fontSize * 18).sp,
        fontWeight = FontWeight.Bold
    )
    val bodyMedium: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 15).sp,
        lineHeight = (fontSize * 18).sp,
        fontWeight = FontWeight.Medium
    )
    val bodyRegular: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 15).sp,
        lineHeight = (fontSize * 18).sp,
        fontWeight = FontWeight.Normal
    )
    // Footnote
    val footnoteBold: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 13).sp,
        lineHeight = ((fontSize * 15)).sp,
        fontWeight = FontWeight.Bold
    )
    val footnoteMedium: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 13).sp,
        lineHeight = ((fontSize * 15)).sp,
        fontWeight = FontWeight.Medium
    )
    val footnoteRegular: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 13).sp,
        lineHeight = ((fontSize * 15)).sp,
        fontWeight = FontWeight.Normal
    )
    // Caption
    val captionBold: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 11).sp,
        lineHeight = (fontSize * 14).sp,
        fontWeight = FontWeight.Bold
    )
    val captionMedium: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 11).sp,
        lineHeight = (fontSize * 14).sp,
        fontWeight = FontWeight.Medium
    )
    val captionRegular: TextStyle = TextStyle(
        fontFamily = fontFamily,
        fontSize = (fontSize * 11).sp,
        lineHeight = (fontSize * 14).sp,
        fontWeight = FontWeight.Normal
    )
    val materialTypography: Typography = Typography(
        h1 = headline,
        h3 = title,
        subtitle1 = subtitle,
        subtitle2 = subtitle,
        body1 = footnoteMedium,
        body2 = footnoteRegular,
        button = bodyBold,
        caption = captionRegular
    )
}
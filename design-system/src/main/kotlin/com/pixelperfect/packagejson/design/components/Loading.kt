package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun Loading(
    modifier: Modifier = Modifier.fillMaxSize(),
    color: Color = AppTheme.colors.primary,
    strokeSize: Dp = AppTheme.size.XS,
    size: Dp = AppTheme.size.XXXL,
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(size),
            color = color,
            strokeWidth = strokeSize,
        )
    }
}

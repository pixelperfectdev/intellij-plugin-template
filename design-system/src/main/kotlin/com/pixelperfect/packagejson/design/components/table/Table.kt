package com.pixelperfect.packagejson.design.components.table

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.pixelperfect.packagejson.design.components.HorizontalDivider
import com.pixelperfect.packagejson.design.components.Loading
import com.pixelperfect.packagejson.design.components.PageListHandler
import com.pixelperfect.packagejson.design.fondation.AppColorPalette.Companion.Black10
import com.pixelperfect.packagejson.design.fondation.AppColorPalette.Companion.Transparent
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.viewstate.TableColumnType
import com.pixelperfect.packagejson.design.viewstate.TableItemViewState
import com.pixelperfect.packagejson.design.viewstate.TableViewState

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun<T, U> Table(
    modifier: Modifier = Modifier,
    viewState: TableViewState<T, U>,

    // Decoration
    displayHeaders: Boolean = true,
    header: @Composable (() -> Unit)? = null,
    footer: @Composable (() -> Unit)? = null,

    // Paging
    onLoadMore: () -> Unit = {},

    // Item
    onHeaderUrlClick: (String) -> Unit = {},
    onItemClick: (U) -> Unit = {},
    itemContent: @Composable (Int, U, List<Int>) -> Unit,
) where T: TableColumnType, U: TableItemViewState<T> {

    val listState = rememberLazyListState()

    LazyColumn(
        modifier = modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        state = listState
    ) {
        stickyHeader {
            if (displayHeaders) {
                TableHeaders(
                    modifier = Modifier
                        .background(AppTheme.colors.background)
                        .background(AppTheme.colors.text.copy(alpha = 0.10f)),
                    columns = viewState.columns,
                    onUrlClick = onHeaderUrlClick,
                )
            } else HorizontalDivider()
        }

        if (header != null) {
            item {
                header.invoke()
                Divider(color = AppTheme.colors.divider, thickness = AppTheme.size.XXXS)
            }
        }

        itemsIndexed(viewState.items) { index, item ->
            val visibleItems = listState.layoutInfo.visibleItemsInfo.map { it.index }
            Box(
                modifier = Modifier
                    .background(color = if (index.mod(2) == 0) Transparent else Black10)
                    .clickable { onItemClick(item) }
            ) {
                itemContent(index, item, visibleItems)
            }
            Divider(color = AppTheme.colors.divider, thickness = AppTheme.size.XXXS)
        }

        if (viewState.page.isLoadingMore) {
            item {
                Box(
                    modifier = Modifier.fillMaxWidth().padding(all = AppTheme.size.L),
                    contentAlignment = Alignment.Center
                ) {
                    Loading(color = AppTheme.colors.text, size = AppTheme.size.XXL)
                }
            }
        }

        if (footer != null) {
            item {
                footer.invoke()
                Divider(color = AppTheme.colors.divider, thickness = AppTheme.size.XXXS)
            }
        }
    }

    if (viewState.page.isPagingEnabled) {
        PageListHandler(
            listState = listState,
            onLoadMore = onLoadMore,
            buffer = 5,
            isLoading = viewState.page.isLoadingMore
        )
    }
}


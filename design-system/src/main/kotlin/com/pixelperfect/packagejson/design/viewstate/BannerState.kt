package com.pixelperfect.packagejson.design.viewstate

sealed class BannerState {
    object Success: BannerState()
    object Warning: BannerState()
    object Error: BannerState()
}
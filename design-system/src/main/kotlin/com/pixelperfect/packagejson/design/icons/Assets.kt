package com.pixelperfect.packagejson.design.icons

enum class Assets {
    IcBug,
    IcCopy,
    IcGit,
    IcHelp,
    IcLink,
    IcNpm,
    ;
}
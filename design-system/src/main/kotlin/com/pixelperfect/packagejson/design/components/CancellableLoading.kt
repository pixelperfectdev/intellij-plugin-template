package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun CancellableLoading(
    modifier: Modifier = Modifier.fillMaxSize(),
    color: Color = AppTheme.colors.primary,
    strokeSize: Dp = AppTheme.size.XS,
    text: String = "Cancel",
    textColor: Color = AppTheme.colors.textTertiary,
    textStyle: TextStyle = AppTheme.typography.footnoteRegular,
    onCancelClick: () -> Unit
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(AppTheme.size.SS, Alignment.CenterVertically)
    ) {
        Loading(
            modifier = Modifier,
            color = color,
            strokeSize = strokeSize,
        )
        Text(
            modifier = Modifier.clickable { onCancelClick() }.padding(all = AppTheme.size.L),
            text = text,
            color = textColor,
            style = textStyle
        )
    }
}
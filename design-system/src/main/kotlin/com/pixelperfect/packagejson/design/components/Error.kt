package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import com.pixelperfect.packagejson.design.fondation.AppTheme

@Composable
fun Error(
    modifier: Modifier = Modifier.fillMaxSize(),
    errorMessage: String,
    refreshText: String = "Refresh",
    onRefreshClick: (() -> Unit)? = null,
    errorColor: Color = AppTheme.colors.textTertiary,
    errorStyle: TextStyle = AppTheme.typography.footnoteRegular,
    refreshColor: Color = AppTheme.colors.textTertiary,
    refreshStyle: TextStyle = AppTheme.typography.captionMedium,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(AppTheme.size.SS, Alignment.CenterVertically)
    ) {
        Text(
            text = errorMessage,
            color = errorColor,
            style = errorStyle
        )
        if (onRefreshClick != null)
            Text(
                modifier = Modifier.clickable { onRefreshClick() }.padding(all = AppTheme.size.L),
                text = refreshText,
                color = refreshColor,
                style = refreshStyle
            )
    }
}
package com.pixelperfect.packagejson.design.components

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import com.pixelperfect.packagejson.design.icons.Assets
import com.pixelperfect.packagejson.design.icons.toPath

@Composable
fun Icon(
    modifier: Modifier = Modifier,
    imageVector: Assets,
    contentDescription: String? = null,
    tint: Color = Color.Unspecified
) {
    val path = imageVector.toPath()
    androidx.compose.material.Icon(
        modifier = modifier,
        painter = painterResource(path),
        tint = tint,
        contentDescription = contentDescription,
    )
}

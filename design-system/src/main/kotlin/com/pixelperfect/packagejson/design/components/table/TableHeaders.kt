package com.pixelperfect.packagejson.design.components.table

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import com.pixelperfect.packagejson.design.components.HorizontalDivider
import com.pixelperfect.packagejson.design.components.Icon
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.icons.Assets
import com.pixelperfect.packagejson.design.viewstate.TableColumnType
import com.pixelperfect.packagejson.design.viewstate.TableColumnViewState

@Composable
fun<T> TableHeaders(
    modifier: Modifier = Modifier,
    columns: List<TableColumnViewState<T>>,
    onUrlClick: (String) -> Unit
) where T: TableColumnType {
    Column(
        modifier = modifier.fillMaxWidth()
    ) {
        HorizontalDivider()

        if (columns.isEmpty()) return@Column

        TableColumn(
            modifier = modifier.height(IntrinsicSize.Min),
            columns = columns,
        ) { column ->
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(AppTheme.size.S, Alignment.CenterHorizontally),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.padding(vertical = AppTheme.size.XS),
                    text = column.title,
                    style = AppTheme.typography.footnoteMedium,
                    color = AppTheme.colors.text,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                if (column.helper != null) {
                    Icon(
                        modifier = Modifier
                            .requiredSize(AppTheme.size.L)
                            .clickable { onUrlClick(column.helper) },
                        imageVector = Assets.IcHelp,
                        contentDescription = "Help",
                        tint = AppTheme.colors.textTertiary
                    )
                }
            }
        }
        HorizontalDivider()
    }
}
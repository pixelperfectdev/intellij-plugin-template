package com.pixelperfect.packagejson.design.components

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.design.icons.Assets

@Composable
fun ActionButton(
    modifier: Modifier = Modifier,
    text: String,
    imageVector: Assets,
    iconTint: Color,
    onClick: () -> Unit = {}
) {
    Tooltip(
        title = text,
    ) {
        Box(
            modifier = modifier
                .aspectRatio(1f)
                .clip(CircleShape)
                .clickable { onClick() }
                .border(width = AppTheme.size.XXS, color = iconTint, CircleShape)
                .padding(all = AppTheme.size.S),
            contentAlignment = Alignment.Center
        ) {
            Icon(
                imageVector = imageVector,
                tint = iconTint
            )
        }
    }
}

<!-- Plugin description -->
## Package Json
The NPM package.json plugin is a tool designed to simplify the management of your Node.js projects.

### Features
- ✍️ Dependency completion: Easily search for dependency name while editing your package.json file.
- 🆙 Version update suggestions: Get quick fix suggestions to update your dependencies.
- 💻 Open associated npmjs page
- ❗️ Display warning in case of vulnerability found in dependencies

### Screenshots
![Screenshot](https://plugins.jetbrains.com/files/22128/screenshot_30a879dc-17fb-4912-873a-8c949906bfeb)
![Screenshot](https://plugins.jetbrains.com/files/22128/screenshot_eb4baef4-f3c4-4d39-a32d-b95062585d49)
![Screenshot](https://plugins.jetbrains.com/files/22128/screenshot_b27c4ff4-d1c6-4260-82b5-33e03076bab8)
![Screenshot](https://plugins.jetbrains.com/files/22128/screenshot_1db8463f-2fd2-4a92-8159-297696e3b2b1)

### More tools
Checkout CI plugins for IntelliJ IDEs:
- [Gitlab](https://plugins.jetbrains.com/plugin/15457-gitlab-ci-pipeline-dashboard)
- [GitHub](https://plugins.jetbrains.com/plugin/20144-github-ci-dashboard)
- [Circle CI](https://plugins.jetbrains.com/plugin/15458-circle-ci-dashboard)
- [Travis CI](https://plugins.jetbrains.com/plugin/20683-travis-ci-dashboard)
- [Bitrise](https://plugins.jetbrains.com/plugin/16222-bitrise-dashboard)
- [Heroku](https://plugins.jetbrains.com/plugin/15535-heroku-dashboard)
- [Netlify](https://plugins.jetbrains.com/plugin/19977-netlify-dashboard)

<!-- Plugin description end -->

import org.jetbrains.changelog.markdownToHTML
fun properties(key: String) = project.findProperty(key).toString()

plugins {
    kotlin("jvm")
    id("idea")
    // Gradle IntelliJ Plugin
    id("org.jetbrains.intellij")
    // Compose
    id("org.jetbrains.compose")
    // Gradle Changelog Plugin
    id("org.jetbrains.changelog")
}

group = properties("pluginGroup")
version = properties("pluginVersion")

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

// Configure Gradle IntelliJ Plugin - read more: https://github.com/JetBrains/gradle-intellij-plugin
intellij {
    pluginName.set(properties("pluginName"))
    version.set(properties("pluginPlatformVersion"))
    type.set(properties("pluginPlatformType"))
    plugins.set(properties("pluginPlatformPlugins").split(',').map(String::trim).filter(String::isNotEmpty))
}

dependencies {
    // Compose
    implementation(compose.desktop.macos_arm64)
    implementation(compose.desktop.macos_x64)
    implementation(compose.desktop.windows_x64)
    implementation(compose.desktop.linux_arm64)
    implementation(compose.desktop.linux_x64)
    // External dependencies
    implementation(Deps.Kotlin.Coroutines.core)
    implementation(Deps.Kotlin.Coroutines.swing)
    // Local modules
    implementation(project(":design-system"))
    implementation(project(":data"))
    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":repository"))
    implementation(project(":service"))
    implementation(project(":navigation"))
    implementation(project(":translation"))

    implementation(project(":feature:npmlist"))
}

// Set the JVM compatibility versions
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = properties("javaVersion")
}
java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

// Configure Gradle Changelog Plugin - read more: https://github.com/JetBrains/gradle-changelog-plugin
changelog {
    version.set(properties("pluginVersion"))
    groups.set(emptyList())
}

tasks {
    // https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin-faq.html#how-to-disable-building-searchable-options
    buildSearchableOptions {
        enabled = false
    }
    patchPluginXml {
        version.set(properties("pluginVersion"))
        sinceBuild.set(properties("pluginSinceBuild"))
        untilBuild.set(properties("pluginUntilBuild"))

        // Extract the <!-- Plugin description --> section from README.md and provide for the plugin's manifest
        pluginDescription.set(
            projectDir.resolve("README.md").readText().lines().run {
                val start = "<!-- Plugin description -->"
                val end = "<!-- Plugin description end -->"

                if (!containsAll(listOf(start, end))) {
                    throw GradleException("Plugin description section not found in README.md:\n$start ... $end")
                }
                subList(indexOf(start) + 1, indexOf(end))
            }.joinToString("\n").run { markdownToHTML(this) }
        )

        // Get the latest available change notes from the changelog file
        changeNotes.set(provider {
            changelog.run {
                getOrNull(properties("pluginVersion")) ?: getLatest()
            }.toHTML()
        })
    }

    signPlugin {
        certificateChain.set(readFile(rootDir, "plugin_chain.crt"))
        privateKey.set(readFile(rootDir, "plugin_sign_plugin_private.pem"))
        password.set(getLocalProperty(rootDir, "PRIVATE_KEY_PASSWORD").toString())
    }

    publishPlugin {
        dependsOn("patchChangelog")
        token.set(getLocalProperty(rootDir, "PUBLISH_TOKEN").toString())
        // pluginVersion is based on the SemVer (https://semver.org) and supports pre-release labels, like 2.1.7-alpha.3
        // Specify pre-release label to publish the plugin in a custom Release Channel automatically. Read more:
        // https://plugins.jetbrains.com/docs/intellij/deployment.html#specifying-a-release-channel
        channels.set(
            listOf(
                properties("pluginVersion")
                    .split('-')
                    .getOrElse(1) { "default" }
                    .split('.')
                    .first()
            )
        )
    }
}

package com.pixelperfect.packagejson.app.interactor

import com.pixelperfect.packagejson.navigation.ExternalNavigator
import com.pixelperfect.packagejson.translation.Translation
import com.pixelperfect.packagejson.common.usecase.FeedbackUseCase

class FeedbackInteractor(
    private val externalNavigator: ExternalNavigator,
    private val translation: Translation,
): FeedbackUseCase {
    override suspend fun invoke() {
        val feedbackUrl = translation.common.feedbackForm
        val url = "$feedbackUrl"
        externalNavigator.navigateToLink(url)
    }
}
package com.pixelperfect.packagejson.app.notification

import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.progress.impl.BackgroundableProcessIndicator
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.IconLoader
import com.pixelperfect.packagejson.app.ui.Icons
import com.pixelperfect.packagejson.common.NotificationActionType
import com.pixelperfect.packagejson.common.NotificationSender
import com.pixelperfect.packagejson.data.BuildSetup

class NotificationSenderImpl(
    private val project: Project,
    buildSetup: BuildSetup,
): NotificationSender {

    private val NOTIFICATION_GROUP = buildSetup.APP_NAME

    override fun notify(
        title: String,
        message: String,
        iconPath: String,
        actions: List<NotificationActionType>,
        callback: (NotificationActionType) -> Unit
    ) {
        NotificationGroupManager.getInstance()
            .getNotificationGroup(NOTIFICATION_GROUP)
            .createNotification(title, message, NotificationType.INFORMATION)
            .setIcon(IconLoader.getIcon(iconPath, Icons::class.java))
            .let { notification ->
                notification.addActions(
                    actions.map { action ->
                        NotificationAction(action, { callback(action) })
                    } as Collection<AnAction>
                )
            }
            .notify(project)


    }

    override fun notifySuccess(title: String, message: String) {
        displayNotification(message, title, NotificationType.INFORMATION)
    }

    override fun notifyWarning(title: String, message: String) {
        displayNotification(message, title, NotificationType.WARNING)
    }

    override fun notifyError(title: String, message: String) {
        displayNotification(message, title, NotificationType.ERROR)
    }

    private fun displayNotification(message: String, title: String, type: NotificationType) {
        NotificationGroupManager.getInstance()
            .getNotificationGroup(NOTIFICATION_GROUP)
            .let {
                if (title.isNotEmpty()) it.createNotification(title, message, type)
                else it.createNotification(message, type)
            }
            .notify(project)
    }

    override fun displayBackgroundProgressIndicator(title: String): Any {
        return BackgroundableProcessIndicator(
            project,
            title,
            null, null, false
        ).apply {
            isIndeterminate = true
        }
    }
}
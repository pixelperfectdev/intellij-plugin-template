package com.pixelperfect.packagejson.app.di

import com.pixelperfect.packagejson.app.PluginViewModel
import com.pixelperfect.packagejson.app.interactor.FeedbackInteractor
import com.pixelperfect.packagejson.app.ui.SwingColors
import com.pixelperfect.packagejson.common.AppInstance
import com.pixelperfect.packagejson.common.KoinName.COROUTINE_IO
import com.pixelperfect.packagejson.common.KoinName.COROUTINE_UI
import com.pixelperfect.packagejson.common.KoinName.DISPATCHER_IO
import com.pixelperfect.packagejson.common.KoinName.DISPATCHER_UI
import com.pixelperfect.packagejson.common.KoinName.DOWNLOAD_FOLDER
import com.pixelperfect.packagejson.common.KoinName.LOG_ROOT_FOLDER
import com.pixelperfect.packagejson.common.NotificationSender
import com.pixelperfect.packagejson.common.commonModule
import com.pixelperfect.packagejson.navigation.navigationModule
import com.pixelperfect.packagejson.translation.Translation
import kotlinx.coroutines.*
import kotlinx.coroutines.swing.Swing
import org.koin.core.qualifier.named
import org.koin.dsl.module
import com.pixelperfect.packagejson.app.notification.NotificationSenderImpl
import com.pixelperfect.packagejson.common.usecase.FeedbackUseCase
import com.pixelperfect.packagejson.common.usecase.MoreExtensionUseCase
import com.pixelperfect.packagejson.app.interactor.MoreExtensionInteractor
import com.pixelperfect.packagejson.data.BuildSetup
import com.pixelperfect.packagejson.domain.presenter.PluginPresenter
import com.pixelperfect.packagejson.app.ui.PluginPresenterImpl
import com.pixelperfect.packagejson.navigation.ExternalNavigator
import com.pixelperfect.packagejson.app.navigation.PluginNavigator
import com.pixelperfect.packagejson.npm.npmListModule
import com.pixelperfect.packagejson.repository.di.repositoryModule
import com.pixelperfect.packagejson.service.di.serviceModule

val appPluginModule = module {

    // Config

    single<BuildSetup> { PluginBuildSetup() }

    // View Model
    scope<AppInstance> {
        scoped<PluginViewModel> {
            PluginViewModel(
                presenter = get(),
                navigationManager = get(),
                navigationSessionManager = get(),
                viewModelDispatcher = Dispatchers.Swing
            )
        }

        scoped<PluginPresenter> { PluginPresenterImpl() }
//
//        factory<UpdateConfigUseCase> {
//            UpdateConfigInteractor(
//                presenter = get(),
//                sessionRepository = get(),
//            )
//        }
//
//        // Documentation / WebView
//        scoped<LoadWebViewUseCase> {
//            LoadWebViewInteractor(presenter = get())
//        }
//        factory<NavigateToHomeUseCase> {
//            NavigateToHomeInteractor(
//                presenter = get(),
//                sessionRepository = get(),
//            )
//        }

        scoped<NotificationSender> { params ->
            NotificationSenderImpl(
                project = params.get(),
                buildSetup = get()
            )
        }
    }

    factory<FeedbackUseCase> {
        FeedbackInteractor(
            externalNavigator = get(),
            translation = get(),
        )
    }

    factory<MoreExtensionUseCase> {
        MoreExtensionInteractor(
            externalNavigator = get()
        )
    }



    // UI
    single<Translation> { Translation() }
    single<SwingColors> { SwingColors() }

    // Navigation

    factory<ExternalNavigator> { PluginNavigator }

    // Coroutines

    factory<CoroutineScope>(named(COROUTINE_IO)) {
        CoroutineScope(
            SupervisorJob()
                    + get<CoroutineDispatcher>(named(DISPATCHER_IO))
                    + get<CoroutineExceptionHandler>()
        )
    }
    factory<CoroutineScope>(named(COROUTINE_UI)) {
        CoroutineScope(
            SupervisorJob()
                    + get<CoroutineDispatcher>(named(DISPATCHER_UI))
                    + get<CoroutineExceptionHandler>()
        )
    }
    factory<CoroutineExceptionHandler> {
        CoroutineExceptionHandler { _, exception ->
            exception.printStackTrace()
        }
    }
    factory<CoroutineDispatcher>(named(DISPATCHER_UI)) { Dispatchers.Swing }
    factory<CoroutineDispatcher>(named(DISPATCHER_IO)) { Dispatchers.IO }

    // Keys

    single<String>(named(LOG_ROOT_FOLDER)) { System.getProperty("java.io.tmpdir") }
    single<String>(named(DOWNLOAD_FOLDER)) { System.getProperty("user.home") }

}

val allModules =
    // Common
    commonModule +
    // Target
    appPluginModule +
    // Feature
    npmListModule +
    // Business
    navigationModule +
    repositoryModule +
    serviceModule

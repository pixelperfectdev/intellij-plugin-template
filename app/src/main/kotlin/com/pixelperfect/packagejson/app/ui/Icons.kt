package com.pixelperfect.packagejson.app.ui

import com.intellij.openapi.util.IconLoader

object Icons {
    val Web = IconLoader.getIcon("/icons/ic_web_16.svg", Icons::class.java)
    val Extension = IconLoader.getIcon("/icons/ic_extension_16.png", Icons::class.java)
    val JsLibrary = IconLoader.getIcon("/icons/ic_js_library_16.png", Icons::class.java)
}



package com.pixelperfect.packagejson.app.ide.editor

import com.intellij.json.psi.JsonElement
import com.intellij.json.psi.JsonObject
import com.intellij.json.psi.JsonProperty
import com.intellij.json.psi.JsonStringLiteral
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.psi.util.siblings

fun PsiElement.isPackageJsonFile(): Boolean {
    return containingFile?.name == "package.json"
}

fun JsonElement?.isParentDependencies(): Boolean {
    val parent = PsiTreeUtil.getParentOfType(this?.parent, JsonProperty::class.java)
    return (parent?.name.equals("dependencies", ignoreCase = true) && parent?.value is JsonObject)
            || (parent?.name.equals("devDependencies", ignoreCase = true) && parent?.value is JsonObject)
}

fun PsiElement.getDependency(): String? {
    return siblings(forward = false, withSelf = false)
        .find { node ->
            node is JsonStringLiteral && node.isPropertyName
        }
        ?.text?.replace("\"", "")?.trim()
}

fun JsonProperty?.propertyName(): String {
    return this?.text
        ?.split(':')
        ?.get(0)
        ?.replace("\"", "")
        ?.trim()
        ?.removeSuffix("IntellijIdeaRulezzz")
        ?: ""
}
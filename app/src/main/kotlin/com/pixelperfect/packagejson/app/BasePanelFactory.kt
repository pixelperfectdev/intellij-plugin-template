package com.pixelperfect.packagejson.app

import com.intellij.openapi.fileChooser.FileChooser
import com.intellij.openapi.fileChooser.FileChooserDescriptor
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.pixelperfect.packagejson.app.component.ComponentFactory
import com.pixelperfect.packagejson.app.component.PluginComponentFactory
import com.pixelperfect.packagejson.app.di.allModules
import com.pixelperfect.packagejson.app.ui.SwingColors
import com.pixelperfect.packagejson.common.KoinName
import com.pixelperfect.packagejson.common.NotificationSender
import com.pixelperfect.packagejson.common.koinInit
import com.pixelperfect.packagejson.design.viewstate.NotificationViewState
import com.pixelperfect.packagejson.domain.presenter.PluginViewState
import com.pixelperfect.packagejson.translation.Translation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.swing.Swing
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

class BasePanelFactory: ToolWindowFactory, KoinComponent {

    private val coroutineScope: CoroutineScope by inject(named(KoinName.COROUTINE_UI))
    private val panelFactories = mutableMapOf<Project, ComponentFactory>()
    private val notificationSenders = mutableMapOf<Project, NotificationSender>()
    private val translation: Translation by inject()
    private val colors: SwingColors by inject()

    init {
        koinInit(allModules)
    }

    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        if (panelFactories[project] != null) return

        // Init new panel
        val panelFactory = PluginComponentFactory(project, toolWindow, colors, translation)
        val pluginViewModel: PluginViewModel by panelFactory.scope.inject()
        panelFactories[project] = panelFactory
        notificationSenders[project] = panelFactory.scope.get { parametersOf(project) }

        // View state
        coroutineScope.launch {
            pluginViewModel.viewState.collect { viewState ->
                withContext(Dispatchers.Swing) {
                    updateToolWindow(panelFactory, project, viewState)
                }
            }
        }

//        // Dialog state
//        coroutineScope.launch {
//            pluginViewModel.dialogState.collect { notificationState ->
//                withContext(Dispatchers.Swing) {
//                    notificationSenders[project]?.also { sender ->
//                        displayNotification(sender, notificationState)
//                    }
//                }
//            }
//        }
//
//        // Event state
//        coroutineScope.launch {
//            pluginViewModel.eventState.collect { eventState ->
//                withContext(Dispatchers.Swing) {
//                    displayEvent(panelFactory, eventState)
//                }
//            }
//        }
    }

    private fun updateToolWindow(panelFactory: ComponentFactory, project: Project, viewState: PluginViewState) {
        when (viewState) {
            else -> {
                panelFactory.displayPanel()
            }
        }
    }

    private fun displayNotification(sender: NotificationSender, notification: NotificationViewState) {
        when (notification) {
            is NotificationViewState.Banner.Default -> sender.notifySuccess(
                notification.title, notification.message
            )
            is NotificationViewState.Banner.Error -> sender.notifyError(
                notification.title, notification.message
            )
            is NotificationViewState.Dialog.FilePicker -> {
                val project = null
                val toSelect = null
                val vf = FileChooser.chooseFile(DEFAULT_FILE_PICKER_FD, project, toSelect)
                notification.onFilePicked(vf?.path)
            }
            NotificationViewState.None,
            NotificationViewState.Dialog.FolderPicker -> {}
        }
    }

//    private fun displayEvent(panelFactory: ComponentFactory, event: EventViewState) {
//        when (event) {
//            EventViewState.None -> {}
//            is EventViewState.RefreshBuildList -> {
//                val buildListViewModel: BuildListViewModel by panelFactory.scope.inject()
//                buildListViewModel.refreshDataList()
//                if (event.requireFocus) panelFactory.displayBuildList(requireFocus = true)
//            }
//            is EventViewState.SelectProject -> {}
//            is EventViewState.DisplayDiscount -> DiscountAction.displayDiscountDialog()
//        }
//    }

    companion object {
        private val DEFAULT_FILE_PICKER_FD = FileChooserDescriptor(
            true,           // chooseFiles
            false,        // chooseFolders
            false,          // chooseJars
            false,     // chooseJarsAsFiles
            false,    // chooseJarContents
            false,       // chooseMultiple
        )
    }
}

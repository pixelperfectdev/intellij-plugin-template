package com.pixelperfect.packagejson.app.ide.editor.url

import com.intellij.json.psi.JsonStringLiteral
import com.intellij.openapi.paths.WebReference
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReference
import com.intellij.psi.PsiReferenceProvider
import com.intellij.util.ProcessingContext
import com.pixelperfect.packagejson.app.ide.editor.getDependency
import com.pixelperfect.packagejson.app.ide.editor.isPackageJsonFile
import com.pixelperfect.packagejson.app.ide.editor.isParentDependencies
import com.pixelperfect.packagejson.app.ide.server.PackageRequestHandler
import com.pixelperfect.packagejson.app.ide.server.PackageRequestHandler.SERVICE_URL
import org.koin.core.component.KoinComponent

object PackageArbitraryPlaceUrlReferenceProvider: PsiReferenceProvider(), KoinComponent {

    private val PKG_URL get() = "$SERVICE_URL${PackageRequestHandler.PACKAGE_PATH}?pkg=%s"
    private val PKG_VERSION_URL get() = "$SERVICE_URL${PackageRequestHandler.PACKAGE_VERSION_PATH}?pkg=%s"

    override fun getReferencesByElement(element: PsiElement, context: ProcessingContext): Array<PsiReference> {
        val result = mutableListOf<PsiReference?>()
        if (element.isPackageJsonFile() && element is JsonStringLiteral && element.isParentDependencies()) {
            result.add(
                if (element.isPropertyName) fetchPackageReference(element)
                else fetchPackageVersionsReference(element)
            )
        }
        return result.filterNotNull().toTypedArray()
    }

    private fun fetchPackageReference(element: PsiElement): WebReference? {
        val key = element.text
        val dependency = key.replace("\"", "").trim()
        val range = SafeRange(key.indexOf('"') + 1, key.lastIndexOf('"'))
        if (range.isEmpty) return null
        else return WebReference(
            element,
            range,
            PKG_URL.format(dependency)
        )
    }

    private fun fetchPackageVersionsReference(element: PsiElement): WebReference? {
        val key = element.text
        return element.getDependency()
            ?.let { dependency ->
                val range = SafeRange(key.indexOf('"') + 1, key.lastIndexOf('"'))
                if (range.isEmpty) return null
                else return WebReference(
                    element,
                    range,
                    PKG_VERSION_URL.format(dependency)
                )
            }
    }
}

class SafeRange(
    startOffset: Int,
    endOffset: Int
): TextRange(
    startOffset,
    endOffset.takeIf { endOffset >= startOffset } ?: startOffset
)
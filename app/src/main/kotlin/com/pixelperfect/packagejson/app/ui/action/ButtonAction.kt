package com.pixelperfect.packagejson.app.ui.action

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.pixelperfect.packagejson.app.ui.ToolbarAction

class ButtonAction(
    private val button: ToolbarAction.Button,
): AnAction(
    button.title,
    button.hint,
    button.icon.resource,
) {
    override fun actionPerformed(event: AnActionEvent) {
        button.onClick()
    }
}
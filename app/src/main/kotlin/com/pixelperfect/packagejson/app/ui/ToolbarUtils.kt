package com.pixelperfect.packagejson.app.ui

import com.intellij.icons.AllIcons
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.ActionToolbar
import com.intellij.openapi.actionSystem.DefaultActionGroup
import com.intellij.openapi.util.IconLoader
import com.pixelperfect.packagejson.app.ui.action.ButtonAction
import com.pixelperfect.packagejson.app.ui.action.TextFieldAction
import com.pixelperfect.packagejson.design.icons.Assets
import com.pixelperfect.packagejson.design.icons.toPath
import javax.swing.Icon

fun setupToolbar(
    id: String,
    onHelpClick: (() -> Unit),
    onExtensionsClick: (() -> Unit),
    vararg actions: ToolbarAction
): ActionToolbar {
    // Setup toolbar
    val actionManager = ActionManager.getInstance()
    val actionGroup = DefaultActionGroup("ACTION_GROUP_$id", false)

    actions.mapNotNull { action ->
        when (action) {
            is ToolbarAction.Button -> ButtonAction(action)
            is ToolbarAction.TextField -> TextFieldAction(action)
            is ToolbarAction.DropDownMenu -> null
        }
    }.forEach { actionGroup.add(it) }

    // Extensions
    actionGroup.add(
        ButtonAction(
            button = ToolbarAction.Button(
                title = "More extensions",
                icon = ActionIcon(Icons.Extension)
            ) {
                onExtensionsClick()
            }
        )
    )

    // Help
    actionGroup.add(
        ButtonAction(
            button = ToolbarAction.Button(
                title = "Help / Feedback",
                icon = ActionIcon(AllIcons.General.ContextHelp)
            ) {
                onHelpClick()
            }
        )
    )

    // Create toolbar
    return actionManager.createActionToolbar("ACTION_TOOLBAR_$id", actionGroup, true)
}

sealed class ToolbarAction {

    data class DropDownMenu(
        val label: String,
        val tooltip: String,
        val items: List<String>,
        val selectedItem: String?,
        val onSelected: (item: String) -> Unit
    ): ToolbarAction()

    data class TextField(
        val id: String,
        val value: String,
        val hint: String,
        val onTextChanged: (String) -> Unit,
        val onSubmitForward: () -> Unit = {},
        val onSubmitBackward: () -> Unit = {}
    ): ToolbarAction()

    data class Button(
        val title: String,
        val hint: String = "",
        val icon: ActionIcon,
        val onClick: () -> Unit
    ): ToolbarAction()
}

data class ActionIcon(val resource: Icon) {
    constructor(asset: Assets): this(asset.toPath())
    constructor(path: String): this(IconLoader.getIcon(path, Icons::class.java))
}

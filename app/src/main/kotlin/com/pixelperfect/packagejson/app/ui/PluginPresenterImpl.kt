package com.pixelperfect.packagejson.app.ui

import com.pixelperfect.packagejson.data.NotificationEvent
import com.pixelperfect.packagejson.domain.presenter.PluginPresenter
import com.pixelperfect.packagejson.domain.presenter.PluginViewState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class PluginPresenterImpl: PluginPresenter {

    private val _viewState = MutableStateFlow<PluginViewState>(PluginViewState.Connect)
    override val viewState = _viewState.asStateFlow()

    private val _dialogState = MutableStateFlow<NotificationEvent>(NotificationEvent.None)
    override val dialogState = _dialogState.asStateFlow()

    // Notifications

    override fun displayNotificationSuccess(title: String, message: String) {
        _dialogState.value = NotificationEvent.Banner(title, message, NotificationEvent.BannerType.Success)
    }

    override fun displayNotificationError(title: String, message: String) {
        _dialogState.value = NotificationEvent.Banner(title, message, NotificationEvent.BannerType.Error)
    }

}
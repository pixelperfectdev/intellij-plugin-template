package com.pixelperfect.packagejson.app.ui

import androidx.compose.runtime.Immutable
import com.pixelperfect.packagejson.design.fondation.AppTypography
import javax.swing.UIManager

@Immutable
class SwingTypography: AppTypography(
    size = UIManager.getFont("Panel.font").size
)

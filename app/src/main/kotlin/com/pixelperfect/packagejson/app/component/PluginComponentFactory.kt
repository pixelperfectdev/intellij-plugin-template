package com.pixelperfect.packagejson.app.component

import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.pixelperfect.packagejson.app.ui.Icons
import com.pixelperfect.packagejson.app.ui.SwingColors
import com.pixelperfect.packagejson.app.ui.ToolbarAction
import com.pixelperfect.packagejson.app.ui.setupToolbar
import com.pixelperfect.packagejson.common.debounce
import com.pixelperfect.packagejson.npm.NpmListScreen
import com.pixelperfect.packagejson.npm.NpmListViewModel
import com.pixelperfect.packagejson.translation.Translation

class PluginComponentFactory(
    project: Project,
    toolWindow: ToolWindow,
    swingColors: SwingColors,
    translation: Translation
): ComponentFactory(project, toolWindow, swingColors, translation) {

    override fun displayPanel() {
        val npmListViewModel: NpmListViewModel = scope.get()
        toolWindow.addTab(
            title = "Projects",
            icon = Icons.Web,
            autoSelectContent = false,
            actionToolbar = {
                val toolbar = npmListViewModel.toolbarState.value
                setupToolbar(
                    id = "NPM",
                    onHelpClick = npmListViewModel::onHelpClick,
                    onExtensionsClick = npmListViewModel::onExtensionClickClick,
                    actions = arrayOf(
                        ToolbarAction.TextField(
                            id = "npm-packages",
                            value = toolbar.searchQuery,
                            hint = "Search npm packages",
                            onTextChanged = debounce(
                                delay = 100,
                                scope = npmListViewModel.viewModelScope,
                                handler = npmListViewModel.handler,
                            ) { npmListViewModel.onSearchUpdate(it) }
                        )
                    ),
                )
            },
            content = {
                NpmListScreen(npmListViewModel)
            }
        )
    }
}

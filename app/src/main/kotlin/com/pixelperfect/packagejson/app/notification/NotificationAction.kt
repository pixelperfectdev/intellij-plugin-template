package com.pixelperfect.packagejson.app.notification

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.pixelperfect.packagejson.common.NotificationActionType
import org.koin.core.component.KoinComponent

class NotificationAction(
    private val notificationActionType: NotificationActionType,
    private val callback: (NotificationActionType) -> Unit
): AnAction(notificationActionType.name), KoinComponent {

    override fun actionPerformed(e: AnActionEvent) {
        callback(notificationActionType)
    }
}
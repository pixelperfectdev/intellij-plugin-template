package com.pixelperfect.packagejson.app.ide.editor.completion

import com.intellij.codeInsight.completion.*
import com.intellij.json.psi.JsonProperty
import com.intellij.patterns.PlatformPatterns

class PackageJsonCompletionContributor: CompletionContributor() {

    init {
        extend(
            CompletionType.BASIC,
            PlatformPatterns.psiElement().inside(JsonProperty::class.java),
            PackageCompletionProvider()
        )
    }
}
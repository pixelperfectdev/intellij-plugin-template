package com.pixelperfect.packagejson.app.ide.editor.completion

import com.intellij.codeInsight.completion.CompletionParameters
import com.intellij.codeInsight.completion.CompletionProvider
import com.intellij.codeInsight.completion.CompletionResultSet
import com.intellij.codeInsight.lookup.LookupElementBuilder
import com.intellij.json.psi.JsonProperty
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.util.ProcessingContext
import com.pixelperfect.packagejson.app.ide.editor.isParentDependencies
import com.pixelperfect.packagejson.app.ide.editor.propertyName
import com.pixelperfect.packagejson.app.ui.Icons
import com.pixelperfect.packagejson.data.npm.NpmPackageInfo
import com.pixelperfect.packagejson.domain.repository.NpmRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PackageCompletionProvider: CompletionProvider<CompletionParameters>(), KoinComponent {

    private val PACKAGE_FILE_NAME = "package.json"
    private val npmRepository: NpmRepository by inject()

    override fun addCompletions(
        parameters: CompletionParameters,
        context: ProcessingContext,
        result: CompletionResultSet
    ) {
        if (!parameters.originalFile.name.equals(PACKAGE_FILE_NAME, ignoreCase = true)) return

        val position = parameters.position
        val element = PsiTreeUtil.getParentOfType(position, JsonProperty::class.java)

        val query = element.propertyName()
        if (element.isParentDependencies() && element is JsonProperty && query.isNotEmpty()) {
            npmRepository.getPackages(query)
                .onSuccess { packages ->
                    packages.onEach { pkg ->
                        result.addElement(
                            LookupElementBuilder
                                .create(pkg.toCompletion(element.text))
                                .withIcon(Icons.JsLibrary)
                                .withPresentableText(pkg.name)
                                .withTailText(" ${pkg.description}")
                        )
                    }
                }
        }
    }

    private fun NpmPackageInfo.toCompletion(query: String): String {
        val prefix = if (query.firstOrNull() == '"') "" else '"'
        val suffix = if (query.length > 1 && query.lastOrNull() == '"') "" else '"'
        return "$prefix${name}\": \"${version}$suffix"
    }
}
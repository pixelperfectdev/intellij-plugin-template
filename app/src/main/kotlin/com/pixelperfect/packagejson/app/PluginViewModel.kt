package com.pixelperfect.packagejson.app

import com.pixelperfect.packagejson.common.ViewModel
import com.pixelperfect.packagejson.domain.presenter.PluginPresenter
import com.pixelperfect.packagejson.navigation.NavigationManager
import com.pixelperfect.packagejson.navigation.NavigationSessionManager
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PluginViewModel(
    private val presenter: PluginPresenter,
    private val navigationManager: NavigationManager,
    private val navigationSessionManager: NavigationSessionManager,
    private val viewModelDispatcher: CoroutineDispatcher
): ViewModel() {

    val viewState = presenter.viewState

    init {
        viewModelScope.launch {
            navigationManager.route.collect { route ->
                withContext(viewModelDispatcher) {
                    when (route) {
                        else -> {}
                    }
                }
            }
        }
    }

    fun onLogout() {
        navigationSessionManager.navigateToLogout()
    }
}
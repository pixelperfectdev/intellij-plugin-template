package com.pixelperfect.packagejson.app.ide.language

import com.intellij.json.JsonLanguage
import com.intellij.openapi.fileTypes.LanguageFileType
import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object NpmFileType: LanguageFileType(JsonLanguage.INSTANCE) {
    override fun getName(): String {
        return "NPM Package json"
    }

    override fun getDescription(): String {
        return "NPM package.json file"
    }

    override fun getDefaultExtension(): String {
        return "package.json"
    }

    override fun getIcon(): Icon {
        return IconLoader.getIcon("/icons/ic_plugin_package_json_13.svg", NpmFileType::class.java)
    }
}
package com.pixelperfect.packagejson.app.ide.editor.quickfix

import com.intellij.codeInspection.*
import com.intellij.json.psi.JsonElementGenerator
import com.intellij.json.psi.JsonStringLiteral
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.lang.annotation.ProblemGroup
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import com.pixelperfect.packagejson.app.ide.editor.getDependency
import com.pixelperfect.packagejson.app.ide.editor.isPackageJsonFile
import com.pixelperfect.packagejson.app.ide.editor.isParentDependencies
import com.pixelperfect.packagejson.domain.repository.NpmRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object VersionAnnotator: Annotator, KoinComponent {

    private val npmRepository: NpmRepository by inject()

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        if (element.isPackageJsonFile() && element is JsonStringLiteral && element.isParentDependencies()) {
            if (element.isPropertyName) {

            }
            else {
                val dependency = element.getDependency() ?: return
                val version = element.text.replace("\"", "").trim()
                npmRepository.latestVersion(dependency, version)
                    .onSuccess { (latestVersion, isCurrent) ->
                        if (!isCurrent) {
                            holder.newAnnotation(
                                HighlightSeverity.WARNING,
                                "A newer version of \"${dependency}\" is available: $latestVersion"
                            )
                                .range(TextRange(element.startOffset, element.endOffset))
                                .newLocalQuickFix(
                                    UpdateVersionQuickFix(element, latestVersion, VersionType.Exact),
                                    VersionProblemDescriptor(element)
                                )
                                .registerFix()
                                .newLocalQuickFix(
                                    UpdateVersionQuickFix(element, latestVersion, VersionType.Approximate),
                                    VersionProblemDescriptor(element)
                                )
                                .registerFix()
                                .newLocalQuickFix(
                                    UpdateVersionQuickFix(element, latestVersion, VersionType.CompatibleWith),
                                    VersionProblemDescriptor(element)
                                )
                                .registerFix()

                                .create()
                        }
                    }
            }
        }
    }
}

class UpdateVersionQuickFix(
    private val element: PsiElement,
    private val newVersion: String,
    private val versionType: VersionType
): LocalQuickFix {
    override fun getFamilyName(): String {
        return versionType.format(newVersion)
    }

    override fun applyFix(project: Project, descriptor: ProblemDescriptor) {
        element.replace(JsonElementGenerator(project).createStringLiteral(versionType.getFix(newVersion)))
    }
}

enum class VersionType {
    Exact,
    Approximate,
    CompatibleWith;

    fun format(version: String): String {
        return when (this) {
            Exact -> "Change to $version"
            Approximate -> "Change to ~$version (approximately equivalent)"
            CompatibleWith -> "Change to ^$version (compatible with)"
        }
    }

    fun getFix(version: String): String {
        return when (this) {
            Exact -> version
            Approximate -> "~$version"
            CompatibleWith -> "^$version"
        }
    }
}

class VersionProblemDescriptor(
    private val element: PsiElement
): ProblemDescriptor {
    override fun getDescriptionTemplate(): String {
        return ""
    }

    override fun getFixes(): Array<QuickFix<CommonProblemDescriptor>> {
        return arrayOf(
            object: QuickFix<CommonProblemDescriptor> {
                override fun getFamilyName(): String {
                    return "Update version"
                }

                override fun applyFix(project: Project, descriptor: CommonProblemDescriptor) {}
            }
        )
    }

    override fun getPsiElement(): PsiElement {
        return element
    }

    override fun getStartElement(): PsiElement {
        return element
    }

    override fun getEndElement(): PsiElement {
        return element
    }

    override fun getTextRangeInElement(): TextRange {
        return TextRange(element.startOffset, element.endOffset)
    }

    override fun getLineNumber(): Int {
        return 0
    }

    override fun getHighlightType(): ProblemHighlightType {
        return ProblemHighlightType.WARNING
    }

    override fun isAfterEndOfLine(): Boolean {
        return false
    }

    override fun setTextAttributes(key: TextAttributesKey?) {

    }

    override fun getProblemGroup(): ProblemGroup? {
        return ProblemGroup { null }
    }

    override fun setProblemGroup(problemGroup: ProblemGroup?) {}

    override fun showTooltip(): Boolean {
        return true
    }
}
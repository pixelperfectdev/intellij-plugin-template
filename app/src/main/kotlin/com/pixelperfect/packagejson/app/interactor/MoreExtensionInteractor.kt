package com.pixelperfect.packagejson.app.interactor

import com.pixelperfect.packagejson.common.usecase.MoreExtensionUseCase
import com.pixelperfect.packagejson.navigation.ExternalNavigator

class MoreExtensionInteractor(
    private val externalNavigator: ExternalNavigator,
): MoreExtensionUseCase {
    override suspend fun invoke() {
        externalNavigator.navigateToLink("https://pixel-perfect.dev")
    }
}
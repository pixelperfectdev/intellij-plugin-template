package com.pixelperfect.packagejson.app.ui.action

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.actionSystem.ex.CustomComponentAction
import com.intellij.openapi.project.DumbAware
import com.intellij.ui.SearchTextField
import com.pixelperfect.packagejson.app.ui.ToolbarAction
import java.awt.Dimension
import java.awt.event.KeyEvent
import java.awt.event.KeyEvent.VK_ENTER
import java.awt.event.KeyListener
import javax.swing.JComponent

class TextFieldAction(
    private val action: ToolbarAction.TextField
) : AnAction(), CustomComponentAction, DumbAware {

    private var lastSearch = action.value
    private val searchField: SearchTextField = object: SearchTextField(true, "${action.id}-search") {
        override fun onFieldCleared() {
            super.onFieldCleared()
            performAction()
        }
    }

    init {
        searchField.preferredSize = Dimension(250, 30)
        searchField.textEditor.text = action.value
        searchField.textEditor.addKeyListener(object: KeyListener {
            override fun keyPressed(e: KeyEvent?) {}
            override fun keyReleased(e: KeyEvent?) {
                when {
                    e == null -> return
                    e.isShiftDown && e.keyCode == VK_ENTER -> action.onSubmitBackward()
                    e.keyCode == VK_ENTER -> action.onSubmitForward()
                    !e.isActionKey -> performAction()
                }
            }
            override fun keyTyped(e: KeyEvent?) {}
        })
        searchField.textEditor.emptyText.text = action.hint
    }

    override fun actionPerformed(e: AnActionEvent) {
        performAction()
    }

    private fun performAction() {
        searchField
            .text
            .takeIf { it != lastSearch }
            ?.also { query ->
                action.onTextChanged(query.trim())
                lastSearch = query.trim()
            }
    }

    override fun createCustomComponent(presentation: Presentation, place: String): JComponent {
        return searchField
    }

    fun reset() {
        searchField.textEditor.text = ""
    }
}
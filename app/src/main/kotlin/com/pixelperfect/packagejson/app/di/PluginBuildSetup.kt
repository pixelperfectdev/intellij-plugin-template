package com.pixelperfect.packagejson.app.di

import com.pixelperfect.packagejson.data.BuildSetup

class PluginBuildSetup: BuildSetup {

    override val APP_ID: String get() = ""
    override val APP_NAME: String get() = ""
    override val APP_VERSION: String get() = ""
    override val DEBUG: Boolean get() = true
    override val AMPLITUDE_API_KEY: String get() = ""
    override val BUGSNAG_KEY: String get() = ""
}
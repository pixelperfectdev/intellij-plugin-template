package com.pixelperfect.packagejson.app.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.awt.ComposePanel
import androidx.compose.ui.awt.SwingPanel
import com.intellij.openapi.actionSystem.ActionToolbar
import com.pixelperfect.packagejson.app.ui.PluginColors
import com.pixelperfect.packagejson.app.ui.SwingColors
import com.pixelperfect.packagejson.app.ui.SwingTypography
import com.pixelperfect.packagejson.design.fondation.AppTheme
import com.pixelperfect.packagejson.translation.Translation
import javax.swing.BoxLayout
import javax.swing.JComponent
import javax.swing.JPanel

fun composePanel(
    content: @Composable () -> Unit,
    translation: Translation,
    actionToolbar: @Composable () -> ActionToolbar? = { null },
    colors: SwingColors
): JComponent {
    return ComposePanel().apply {
        setContent {
            AppTheme(
                colors = PluginColors(colors),
                typography = SwingTypography(),
                translation = translation,
            ) {
                Column(
                    modifier = Modifier.background(color = AppTheme.colors.background),
                ) {
                    val toolbar = actionToolbar.invoke()
                    if (toolbar != null)
                    SwingPanel(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(AppTheme.size.ToolbarHeight),
                        background = AppTheme.colors.error,
                        factory = {
                            JPanel().apply {
                                layout = BoxLayout(this, BoxLayout.Y_AXIS)
                                toolbar.targetComponent = this
                                add(toolbar.component)
                            }
                        }
                    )
                    Box(
                        modifier = Modifier.weight(1f)
                    ) {
                        content()
                    }
                }
            }
        }
    }
}
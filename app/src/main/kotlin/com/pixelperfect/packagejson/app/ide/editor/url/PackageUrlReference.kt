package com.pixelperfect.packagejson.app.ide.editor.url

import com.intellij.json.psi.JsonStringLiteral
import com.intellij.patterns.PsiElementPattern
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiReferenceContributor
import com.intellij.psi.PsiReferenceRegistrar
import com.intellij.util.ProcessingContext

class PackageUrlReference : PsiReferenceContributor() {
    override fun registerReferenceProviders(registrar: PsiReferenceRegistrar) {
        registrar.registerReferenceProvider(object : PsiElementPattern.Capture<PsiElement>(PsiElement::class.java) {
            override fun accepts(o: Any?, context: ProcessingContext): Boolean {
                return o is JsonStringLiteral
            }
        }, PackageArbitraryPlaceUrlReferenceProvider, PsiReferenceRegistrar.LOWER_PRIORITY)
    }
}
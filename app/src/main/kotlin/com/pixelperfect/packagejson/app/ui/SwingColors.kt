package com.pixelperfect.packagejson.app.ui

import androidx.compose.material.CheckboxDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import com.intellij.ide.ui.LafManager
import com.intellij.ide.ui.LafManagerListener
import com.intellij.openapi.application.ApplicationManager
import com.pixelperfect.packagejson.design.fondation.AppColorPalette
import javax.swing.UIManager
import java.awt.Color as AWTColor

internal class ThemeChangeListener(
    val updateColors: () -> Unit
) : LafManagerListener {
    override fun lookAndFeelChanged(source: LafManager) {
        updateColors()
    }
}

@Composable
fun PluginColors(color: SwingColors): AppColorPalette {
    val swingColor = remember { color }

    val messageBus = remember {
        ApplicationManager.getApplication().messageBus.connect()
    }

    remember(messageBus) {
        messageBus.subscribe(
            LafManagerListener.TOPIC,
            ThemeChangeListener(swingColor::updateCurrentColors)
        )
    }

    DisposableEffect(messageBus) {
        onDispose {
            messageBus.disconnect()
        }
    }

    return swingColor
}

open class SwingColors: AppColorPalette() {
    private val _backgroundState: MutableState<Color> = mutableStateOf(getBackgroundColor)
    private val _foregroundState: MutableState<Color> = mutableStateOf(getForegroundColor)

    override val background: Color get() = _backgroundState.value
    override val text: Color get() = _foregroundState.value
    override val divider: Color get() = _foregroundState.value.copy(alpha = 0.20f)
    override val textTertiary: Color get() = _foregroundState.value.copy(alpha = 0.50f)

    private val getBackgroundColor get() = getColor(BACKGROUND_KEY)
    private val getForegroundColor get() = getColor(FOREGROUND_KEY)

    override val signinLogo: Color get() = text

    fun updateCurrentColors() {
        _backgroundState.value = getBackgroundColor
        _foregroundState.value = getForegroundColor
    }

    private val AWTColor.asComposeColor: Color get() = Color(red, green, blue, alpha)
    private fun getColor(key: String): Color = UIManager.getColor(key).asComposeColor

    @Composable
    override fun checkBoxColors() = CheckboxDefaults.colors(
        checkedColor = text,
        uncheckedColor = text,
        checkmarkColor = background,
        disabledColor = text,
        disabledIndeterminateColor = text,
    )


    companion object {
        private const val BACKGROUND_KEY = "Panel.background"
        private const val FOREGROUND_KEY = "Panel.foreground"
        private const val PANEL_FONT = "Panel.font"
    }
}
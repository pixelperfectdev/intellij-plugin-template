package com.pixelperfect.packagejson.app.ide.tree

import com.intellij.icons.AllIcons
import com.intellij.ide.projectView.PresentationData
import com.intellij.ide.projectView.ProjectViewNode
import com.intellij.ide.projectView.TreeStructureProvider
import com.intellij.ide.projectView.ViewSettings
import com.intellij.ide.projectView.impl.nodes.ProjectViewProjectNode
import com.intellij.ide.util.treeView.AbstractTreeNode
//import com.intellij.navigation.NavigationRequest
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.ui.SimpleTextAttributes
import com.pixelperfect.packagejson.app.ui.Icons
import com.pixelperfect.packagejson.data.npm.NpmDependency
import com.pixelperfect.packagejson.domain.repository.NpmRepository
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object DependencyTreeStructureProvider: TreeStructureProvider, KoinComponent {

    private val npmRepository: NpmRepository by inject()

    override fun modify(
        parent: AbstractTreeNode<*>,
        children: MutableCollection<AbstractTreeNode<*>>,
        settings: ViewSettings?
    ): MutableCollection<AbstractTreeNode<*>> {
        if (parent is ProjectViewProjectNode) {

            val modifiedChildren: MutableList<AbstractTreeNode<*>> = ArrayList()
            for (child in children) {
                modifiedChildren.add(child)
            }

            modifiedChildren.add(getDependencyNodes(parent.project, settings))
            // Add additional child nodes for sub-dependencies
            return modifiedChildren
        }
        return children

    }

    private fun getDependencyNodes(
        project: Project,
        viewSettings: ViewSettings?
    ): AbstractTreeNode<*> {
        val dependencies: List<NpmDependency>
        runBlocking {
            dependencies = listOf(
//                npmRepository.getDependencies("nuxt", "2.17.0")
            )
        }
        return RootDependencyNode(project, viewSettings, dependencies)
    }
}

class RootDependencyNode(
    project: Project,
    val viewSettings: ViewSettings?,
    val dependencies: List<NpmDependency>,
): ProjectViewNode<List<NpmDependency>>(project, dependencies, viewSettings) {

    override fun update(presentation: PresentationData) {
        presentation.addText("Dependencies", SimpleTextAttributes.REGULAR_ATTRIBUTES)
        presentation.setIcon(AllIcons.Nodes.ModuleGroup)
    }

    override fun getChildren(): MutableCollection<out AbstractTreeNode<*>> {
        return dependencies.map { DependencyNode(project, viewSettings, it) }.toMutableList()
    }

    override fun contains(file: VirtualFile): Boolean {
        return false
    }
}

class DependencyNode(
    project: Project,
    val viewSettings: ViewSettings?,
    val dependency: NpmDependency,
): ProjectViewNode<NpmDependency>(project, dependency, viewSettings) {

    override fun update(presentation: PresentationData) {
        presentation.addText("${dependency.name}:${dependency.version}", SimpleTextAttributes.REGULAR_ATTRIBUTES)
        presentation.setIcon(Icons.JsLibrary)
    }

    override fun getChildren(): MutableCollection<out AbstractTreeNode<*>> {
        return if (dependency.dependencies.isEmpty()) mutableListOf()
        else dependency.dependencies.map { DependencyNode(project, viewSettings, it) }.toMutableList()
    }

//    override fun navigationRequest(): NavigationRequest? {
//        return super.navigationRequest()
//    }

    override fun contains(file: VirtualFile): Boolean {
        return false
    }
}
package com.pixelperfect.packagejson.app.navigation

import com.intellij.ide.BrowserUtil
import com.pixelperfect.packagejson.common.runCatchingCancellable
import com.pixelperfect.packagejson.navigation.ExternalNavigator
import java.net.URI

object PluginNavigator: ExternalNavigator {
    override fun navigateToLink(url: String) {
        runCatchingCancellable {
            BrowserUtil.browse(URI(url))
        }
    }
}
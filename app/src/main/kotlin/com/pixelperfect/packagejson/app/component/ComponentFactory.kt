package com.pixelperfect.packagejson.app.component

import androidx.compose.runtime.Composable
import com.intellij.openapi.actionSystem.ActionToolbar
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.wm.ToolWindow
import com.intellij.ui.content.Content
import com.intellij.ui.content.ContentFactory
import com.pixelperfect.packagejson.app.ui.SwingColors
import com.pixelperfect.packagejson.common.AppInstance
import com.pixelperfect.packagejson.common.KoinName
import com.pixelperfect.packagejson.common.listener.CloseDialogListener
import com.pixelperfect.packagejson.translation.Translation
import kotlinx.coroutines.CoroutineScope
import org.koin.core.component.KoinScopeComponent
import org.koin.core.component.inject
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import java.awt.BorderLayout
import java.awt.Dimension
import javax.swing.Action
import javax.swing.Icon
import javax.swing.JComponent
import javax.swing.JPanel

abstract class ComponentFactory(
    val project: Project,
    val toolWindow: ToolWindow,
    private var swingColors: SwingColors,
    protected var translation: Translation
): KoinScopeComponent {

    private val appInstance = AppInstance()
    override val scope: Scope get() = appInstance.scope

    protected val swingCoroutine: CoroutineScope by inject(named(KoinName.COROUTINE_UI))

    abstract fun displayPanel()

    protected fun ToolWindow.addTab(
        title: String,
        actionToolbar: @Composable () -> ActionToolbar? = { null },
        canClose: Boolean = false,
        icon: Icon? = null,
        autoSelectContent: Boolean = false,
        content: @Composable () -> Unit
    ): Content {
        var tabIndex: Int = -1
        var wasSelected = false
        contentManager.contents.forEachIndexed { index, contentPanel ->
            if (contentPanel != null && contentPanel.tabName == title) {
                wasSelected = contentPanel.isSelected
                contentManager.removeContent(contentPanel, false)
                tabIndex = index
                return@forEachIndexed
            }
        }

        val contentFactory = ContentFactory.getInstance()
        val composePanel = composePanel(content, translation, actionToolbar, swingColors)
        val contentPanel = contentFactory.createContent(composePanel, title, false)
        contentPanel.isCloseable = canClose

        contentPanel.putUserData(ToolWindow.SHOW_CONTENT_ICON, true)
        contentPanel.icon = icon

        if (tabIndex < 0) contentManager.addContent(contentPanel)
        else contentManager.addContent(contentPanel, tabIndex)

        if (autoSelectContent || wasSelected)
            contentManager.setSelectedContent(contentPanel)

        return contentPanel
    }

    fun showDialog(
        title: String,
        width: Int = 620,
        height: Int = 340,
        onClose: () -> Unit,
        content: @Composable () -> Unit,
    ): SimpleDialog {
        return object: SimpleDialog(onClose) {
            init {
                this.title = title
                this.setSize(width, height)
                init()
            }

            override fun createCenterPanel(): JComponent {
                return JPanel(BorderLayout())
                    .apply {
                        val composePanel = composePanel(
                            content = content,
                            translation = translation,
                            colors = swingColors,
                        )
                        composePanel.preferredSize = Dimension(width, height)
                        add(composePanel, BorderLayout.CENTER)
                    }
            }
        }
    }
}

abstract class SimpleDialog(
    private val onClose: () -> Unit
): DialogWrapper(false), CloseDialogListener {

    override fun createActions(): Array<Action> {
        return emptyArray()
    }

    override fun onCloseRequest() {
        close(OK_EXIT_CODE)
        onClose()
    }

    override fun doOKAction() {
        super.doOKAction()
        onClose()
    }

    override fun doCancelAction() {
        super.doCancelAction()
        onClose()
    }
}

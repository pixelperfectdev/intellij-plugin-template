package com.pixelperfect.packagejson.app.ide.server

import com.pixelperfect.packagejson.data.npm.NpmPackageInfo
import com.pixelperfect.packagejson.domain.repository.NpmRepository
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.http.*
import kotlinx.coroutines.runBlocking
import org.jetbrains.builtInWebServer.BuiltInServerOptions
import org.jetbrains.ide.RestService
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object PackageRequestHandler: RestService(), KoinComponent {

    val SERVICE_URL get() = "http://127.0.0.1:${BuiltInServerOptions.getInstance().effectiveBuiltInServerPort}"

    private val SERVICE_NAME get() = "npm-package-json"
    val PACKAGE_PATH get() = "/api.$SERVICE_NAME/package"
    val PACKAGE_VERSION_PATH get() = "/api.$SERVICE_NAME/package-version"

    private val NPM_PKG_URL = "https://npmjs.com/package/%s"
    private val NPM_PKG_VERSION_URL = "https://npmjs.com/package/%s?activeTab=versions"
    private val NPM_PKG_SEARCH_URL = "https://www.npmjs.com/search?q=%s"

    private val npmRepository: NpmRepository by inject()

    override fun execute(
        urlDecoder: QueryStringDecoder,
        request: FullHttpRequest,
        context: ChannelHandlerContext
    ): String? {
        val response = DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.PERMANENT_REDIRECT)

        val pkgQuery = urlDecoder.parameters()["pkg"]?.firstOrNull()
        when (urlDecoder.path()) {
            PACKAGE_PATH -> {
                getPackage(pkgQuery)?.let {
                    response.headers().set("Location", NPM_PKG_URL.format(it.name))
                } ?: run { response.headers().set("Location", NPM_PKG_SEARCH_URL.format(pkgQuery)) }
            }
            PACKAGE_VERSION_PATH -> {
                getPackage(pkgQuery)?.let {
                    response.headers().set("Location", NPM_PKG_VERSION_URL.format(it.name))
                } ?: run { response.headers().set("Location", NPM_PKG_SEARCH_URL.format(pkgQuery)) }
            }
        }

        sendResponse(request, context, response)
        return null
    }

    override fun getServiceName(): String = SERVICE_NAME

    private fun getPackage(query: String?): NpmPackageInfo? {
        return runBlocking {
            if (query == null) null
            else npmRepository.fetchPackage(query).getOrNull()
        }
    }
}
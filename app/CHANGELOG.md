# Package Json plugin changelog

## Unreleased

## 2023.0.8

### Update
- 🆙 Update plugin compatibility with IDE 2024.2

## 2023.0.7

### Update
- 🆙 Update plugin compatibility with IDE 2024.1

## 2023.0.6

### Update
- 🆙 Update plugin compatibility with IDE 2023.3

## 2023.0.5

### Add
- 🆕️ Add support for `devDependencies`.

## 2023.0.4

### Update
- 🆙 Update plugin compatibility with IDE 2023.2

## 2023.0.3

### Add
- ❗️ Display warning in case of vulnerability found in dependencies

### Fix
- 🔨 Fix issue with dependency suggestions

## 2023.0.2

### Add
- ✍️ Dependency completion: Easily search for dependency name while editing your package.json file.
- 🆙 Version update suggestions: Get quick fix suggestions to update your dependencies.
- 💻 Open associated npmjs page

## 2023.0.1

### Add
- 🌠 First release

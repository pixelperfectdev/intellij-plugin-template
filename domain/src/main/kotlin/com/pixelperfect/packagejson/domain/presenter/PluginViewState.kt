package com.pixelperfect.packagejson.domain.presenter

sealed class PluginViewState(val isConnected: Boolean = true) {

    // Signin
    object Connect: PluginViewState(isConnected = false)

    // Main
    sealed class DashboardPanel: PluginViewState() {
        object Idle: DashboardPanel()
        object ListProjects: DashboardPanel()
    }
}

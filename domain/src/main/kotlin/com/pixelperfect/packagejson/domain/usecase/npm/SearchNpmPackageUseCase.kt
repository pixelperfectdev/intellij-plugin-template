package com.pixelperfect.packagejson.domain.usecase.npm

interface SearchNpmPackageUseCase {
    suspend operator fun invoke(query: String)
}
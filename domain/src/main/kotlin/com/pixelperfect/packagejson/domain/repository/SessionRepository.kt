package com.pixelperfect.packagejson.domain.repository

interface SessionRepository: Repository {

    fun isConnected(): Boolean
    suspend fun setDarkTheme(enabled: Boolean)
    suspend fun isDarkThemeEnabled(): Boolean

}
package com.pixelperfect.packagejson.domain.presenter

import com.pixelperfect.packagejson.data.NotificationEvent
import kotlinx.coroutines.flow.StateFlow

interface PluginPresenter {

    val viewState: StateFlow<PluginViewState>
    val dialogState: StateFlow<NotificationEvent>

    // Notification
    fun displayNotificationSuccess(title: String, message: String)
    fun displayNotificationError(title: String, message: String)
}
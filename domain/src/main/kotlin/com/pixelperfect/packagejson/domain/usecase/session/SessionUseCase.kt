package com.pixelperfect.packagejson.domain.usecase.session

interface SessionUseCase {

    suspend fun stop()
}
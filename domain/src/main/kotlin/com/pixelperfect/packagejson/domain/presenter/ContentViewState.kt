package com.pixelperfect.packagejson.domain.presenter

data class ContentViewState(
    val message: String? = null
)


data class ErrorContentViewState(
    val message: String? = null
)
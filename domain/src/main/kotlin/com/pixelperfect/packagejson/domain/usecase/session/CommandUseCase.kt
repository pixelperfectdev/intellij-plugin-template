package com.pixelperfect.packagejson.domain.usecase.session

interface CommandUseCase {

    suspend fun close()
    suspend fun openSettings()

}
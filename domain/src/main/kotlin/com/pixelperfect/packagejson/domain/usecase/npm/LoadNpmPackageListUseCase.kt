package com.pixelperfect.packagejson.domain.usecase.npm

interface LoadNpmPackageListUseCase {
    suspend operator fun invoke(imageId: String)
}
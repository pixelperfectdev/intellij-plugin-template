package com.pixelperfect.packagejson.domain.repository

import com.pixelperfect.packagejson.data.npm.*

interface NpmRepository: Repository {

    // Package
    fun getPackage(packageName: String, version: String? = null): Result<NpmPackage>
    suspend fun fetchPackage(packageName: String, version: String? = null): Result<NpmPackage>

    // Packages
    fun getPackages(query: String): Result<List<NpmPackageInfo>>
    suspend fun searchPackages(query: String): Result<List<NpmPackageSearchResult>>

    // Version
    fun latestVersion(packageName: String, version: String): Result<LatestVersion>

    // Dependencies
    suspend fun getDependencies(packageName: String, version: String): NpmDependency

    // Vulnerability
    fun checkVulnerabilities(packageName: String, version: String): Result<NpmPackageVulnerabilities>

}
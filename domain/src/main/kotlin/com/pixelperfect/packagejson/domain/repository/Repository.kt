package com.pixelperfect.packagejson.domain.repository

interface Repository {
    suspend fun clear()
}
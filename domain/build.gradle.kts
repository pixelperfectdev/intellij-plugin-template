plugins {
    id("multiplatform-setup")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":data"))
}
plugins {
    id("multiplatform-setup")
    kotlin("plugin.serialization")
}

dependencies {
    implementation(Deps.Kotlin.serialization)
}

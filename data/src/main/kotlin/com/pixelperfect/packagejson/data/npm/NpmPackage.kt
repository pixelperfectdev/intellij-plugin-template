package com.pixelperfect.packagejson.data.npm

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NpmPackage(
    @SerialName("name") val _name: String? = null,
    @SerialName("version") val _version: String? = null,
    @SerialName("description") val _description: String? = null,
    @SerialName("versions") val versions: Map<String, NpmVersion>? = null,
    @SerialName("keywords") val keywords: List<String?>? = null,
    @SerialName("dependencies") val dependencies: Map<String, String>? = null,
    @SerialName("devDependencies") val devDependencies: Map<String, String>? = null,
    @SerialName("date") val date: String? = null,
    @SerialName("links") val links: Links? = null,
    @SerialName("publisher") val publisher: Publisher? = null,
    @SerialName("maintainers") val _maintainers: List<NpmPackageInfo.Maintainer?>? = null,
    @SerialName("error") val error: String? = null
): NpmPackageInfo {

    override val name: String get() = _name ?: ""
    override val description: String get() = _description ?: ""
    override val version: String get() = _version ?: ""
    override val maintainers: List<NpmPackageInfo.Maintainer> get() = _maintainers?.filterNotNull() ?: emptyList()

    @Serializable
    data class Links(
        @SerialName("npm") val npm: String? = null,
        @SerialName("homepage") val homepage: String? = null,
        @SerialName("repository") val repository: String? = null,
        @SerialName("bugs") val bugs: String? = null
    )

    @Serializable
    data class Publisher(
        @SerialName("username") val username: String? = null,
        @SerialName("email") val email: String? = null
    )
}
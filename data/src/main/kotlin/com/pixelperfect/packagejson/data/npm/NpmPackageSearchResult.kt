package com.pixelperfect.packagejson.data.npm

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NpmPackageSearchResult(
    @SerialName("package") val _package: Package? = null,
    @SerialName("score") val score: Score? = null,
    @SerialName("searchScore") val searchScore: Double? = null
): NpmPackageInfo {

    override val name: String = _package?.name ?: ""
    override val description: String = _package?.description ?: ""
    override val version: String = _package?.version ?: ""
    override val maintainers: List<NpmPackageInfo.Maintainer> = _package?.maintainers?.filterNotNull() ?: emptyList()
    val dependencyCode get() = "\"${_package?.name}\": \"${_package?.version}\""
    val tags get() = _package?.keywords?.filterNotNull() ?: emptyList()
    val date: String? get() = _package?.date
    val homePageUrl: String? get() = _package?.links?.homepage
    val repositoryUrl: String? get() = _package?.links?.repository
    val npmUrl: String? get() = _package?.links?.npm
    val bugsUrl: String? get() = _package?.links?.bugs


    @Serializable data class Package(
        @SerialName("name") val name: String? = null,
        @SerialName("version") val version: String? = null,
        @SerialName("description") val description: String? = null,
        @SerialName("versions") val versions: Map<String, NpmVersion>? = null,
        @SerialName("keywords") val keywords: List<String?>? = null,
        @SerialName("date") val date: String? = null,
        @SerialName("links") val links: Links? = null,
        @SerialName("publisher") val publisher: Publisher? = null,
        @SerialName("maintainers") val maintainers: List<NpmPackageInfo.Maintainer?>? = null
    ) {
        @Serializable data class Links(
            @SerialName("npm") val npm: String? = null,
            @SerialName("homepage") val homepage: String? = null,
            @SerialName("repository") val repository: String? = null,
            @SerialName("bugs") val bugs: String? = null
        )

        @Serializable data class Publisher(
            @SerialName("username") val username: String? = null,
            @SerialName("email") val email: String? = null
        )
    }

    @Serializable data class Score(
        @SerialName("final") val `final`: Double? = null,
        @SerialName("detail") val detail: Detail? = null
    ) {
        @Serializable data class Detail(
            @SerialName("quality") val quality: Double? = null,
            @SerialName("popularity") val popularity: Double? = null,
            @SerialName("maintenance") val maintenance: Double? = null
        )
    }
}
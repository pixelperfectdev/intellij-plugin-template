package com.pixelperfect.packagejson.data.npm

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NpmSearchResponse(
    @SerialName("objects") val objects: List<NpmPackageSearchResult>? = null,
    @SerialName("total") val total: Int? = null,
    @SerialName("time") val time: String? = null
)
package com.pixelperfect.packagejson.data.npm


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NpmAuditResponse(
    @SerialName("actions") val actions: List<Action?>? = null,
    @SerialName("advisories") val advisories: Map<String, Vulnerability>? = null,
    @SerialName("metadata") val metadata: Metadata? = null
) {
    @Serializable
    data class Action(
        @SerialName("isMajor") val isMajor: Boolean? = null,
        @SerialName("action") val action: String? = null,
        @SerialName("resolves") val resolves: List<Resolve?>? = null,
        @SerialName("module") val module: String? = null,
        @SerialName("target") val target: String? = null
    ) {
        @Serializable
        data class Resolve(
            @SerialName("id") val id: Int? = null,
            @SerialName("path") val path: String? = null,
            @SerialName("dev") val dev: Boolean? = null,
            @SerialName("optional") val optional: Boolean? = null,
            @SerialName("bundled") val bundled: Boolean? = null
        )
    }

    @Serializable
    data class Metadata(
        @SerialName("vulnerabilities") val vulnerabilities: Vulnerabilities? = null,
        @SerialName("dependencies") val dependencies: Int? = null,
        @SerialName("devDependencies") val devDependencies: Int? = null,
        @SerialName("optionalDependencies") val optionalDependencies: Int? = null,
        @SerialName("totalDependencies") val totalDependencies: Int? = null
    ) {
        @Serializable
        data class Vulnerabilities(
            @SerialName("info") val info: Int? = null,
            @SerialName("low") val low: Int? = null,
            @SerialName("moderate") val moderate: Int? = null,
            @SerialName("high") val high: Int? = null,
            @SerialName("critical") val critical: Int? = null
        )
    }

    @Serializable
    data class NpmUser(
        @SerialName("link") val link: String? = null,
        @SerialName("name") val name: String? = null,
        @SerialName("email") val email: String? = null
    )
}

@Serializable
data class Vulnerability(
    @SerialName("id") val id: Int? = null,
    @SerialName("url") val url: String? = null,
    @SerialName("title") val title: String? = null,
    @SerialName("overview") val overview: String? = null,
    @SerialName("recommendation") val recommendation: String? = null,
    @SerialName("severity") val severity: String? = null,
    @SerialName("vulnerable_versions") val vulnerableVersions: String? = null,
    @SerialName("patched_versions") val patchedVersions: String? = null,

    @SerialName("findings") val findings: List<Finding?>? = null,
    @SerialName("module_name") val moduleName: String? = null,
    @SerialName("github_advisory_id") val githubAdvisoryId: String? = null,
    @SerialName("cves") val cves: List<String?>? = null,
    @SerialName("access") val access: String? = null,
    @SerialName("cvss") val cvss: Cvss? = null,
    @SerialName("updated") val updated: String? = null,
    @SerialName("cwe") val cwe: List<String?>? = null,
    @SerialName("found_by") val foundBy: NpmAuditResponse.NpmUser? = null,
    @SerialName("reported_by") val reportedBy: NpmAuditResponse.NpmUser? = null,
    @SerialName("references") val references: String? = null,
    @SerialName("created") val created: String? = null,
) {
    @Serializable
    data class Finding(
        @SerialName("version") val version: String? = null,
        @SerialName("paths") val paths: List<String?>? = null
    )

    @Serializable
    data class Cvss(
        @SerialName("score") val score: Double? = null,
        @SerialName("vectorString") val vectorString: String? = null
    )
}
package com.pixelperfect.packagejson.data.npm


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

fun String?.cleanVersion(): String? {
    if (this == null)return null
    val regex = Regex("\\b(?:\\d+(?:\\.\\d+){0,2})\\b")
    val matchResult = regex.findAll(this)
    return matchResult.firstOrNull()?.value
}

@Serializable
data class NpmVersion(
    @SerialName("name") val name: String? = null,
    @SerialName("version") val version: String? = null,
    @SerialName("description") val description: String? = null,
    @SerialName("main") val main: String? = null,
    @SerialName("license") val license: String? = null,
    @SerialName("repository") val repository: Repository? = null,
    @SerialName("files") val files: List<String?>? = null,
    @SerialName("bin") val bin: Bin? = null,
    @SerialName("scripts") val scripts: Scripts? = null,
    @SerialName("ava") val ava: Ava? = null,
    @SerialName("standard") val standard: Standard? = null,
    @SerialName("dependencies") val dependencies: Map<String, String>? = null,
    @SerialName("devDependencies") val devDependencies: Map<String, String>? = null,
    @SerialName("gitHead") val gitHead: String? = null,
    @SerialName("bugs") val bugs: Bugs? = null,
    @SerialName("homepage") val homepage: String? = null,
    @SerialName("_id") val id: String? = null,
    @SerialName("_shasum") val shasum: String? = null,
    @SerialName("_from") val from: String? = null,
    @SerialName("_npmVersion") val npmVersion: String? = null,
    @SerialName("_nodeVersion") val nodeVersion: String? = null,
    @SerialName("_npmUser") val npmUser: NpmUser? = null,
    @SerialName("maintainers") val maintainers: List<Maintainer?>? = null,
    @SerialName("dist") val dist: Dist? = null,
    @SerialName("_npmOperationalInternal") val npmOperationalInternal: NpmOperationalInternal? = null,
) {
    @Serializable
    data class Repository(
        @SerialName("type") val type: String? = null,
        @SerialName("url") val url: String? = null
    )

    @Serializable
    data class Bin(
        @SerialName("next") val next: String? = null
    )

    @Serializable
    data class Scripts(
        @SerialName("build") val build: String? = null,
        @SerialName("test") val test: String? = null,
        @SerialName("lint") val lint: String? = null
    )

    @Serializable
    data class Ava(
        @SerialName("babel") val babel: Babel? = null
    ) {
        @Serializable
        data class Babel(
            @SerialName("presets") val presets: List<String?>? = null,
            @SerialName("plugins") val plugins: List<String?>? = null
        )
    }

    @Serializable
    data class Standard(
        @SerialName("parser") val parser: String? = null
    )
    @Serializable
    data class Bugs(
        @SerialName("url") val url: String? = null
    )

    @Serializable
    data class NpmUser(
        @SerialName("name") val name: String? = null,
        @SerialName("email") val email: String? = null
    )

    @Serializable
    data class Maintainer(
        @SerialName("name") val name: String? = null,
        @SerialName("email") val email: String? = null
    )

    @Serializable
    data class Dist(
        @SerialName("shasum") val shasum: String? = null,
        @SerialName("tarball") val tarball: String? = null,
        @SerialName("integrity") val integrity: String? = null,
        @SerialName("signatures") val signatures: List<Signature?>? = null
    ) {
        @Serializable
        data class Signature(
            @SerialName("keyid") val keyid: String? = null,
            @SerialName("sig") val sig: String? = null
        )
    }

    @Serializable
    data class NpmOperationalInternal(
        @SerialName("host") val host: String? = null,
        @SerialName("tmp") val tmp: String? = null
    )
}
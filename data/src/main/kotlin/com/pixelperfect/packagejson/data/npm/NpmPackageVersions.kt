package com.pixelperfect.packagejson.data.npm


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NpmPackageVersions(
    @SerialName("_id") val _id: String? = null,
    @SerialName("_rev") val rev: String? = null,
    @SerialName("name") val _name: String? = null,
    @SerialName("description") val _description: String? = null,
    @SerialName("dist-tags") val distTags: DistTags? = null,
    @SerialName("versions") val versions: Map<String, NpmPackage>? = null,
    @SerialName("maintainers") val _maintainers: List<NpmPackageInfo.Maintainer?>? = null,
    @SerialName("author") val author: Author? = null,
    @SerialName("repository") val repository: Repository? = null,
    @SerialName("error") val error: String? = null,
): NpmPackageInfo {

    override val name: String get() = _name ?: ""
    override val description: String get() = _description ?: ""
    override val version: String get() = distTags?.latest ?: ""
    override val maintainers: List<NpmPackageInfo.Maintainer> get() = _maintainers?.filterNotNull() ?: emptyList()

    @Serializable
    data class DistTags(
        @SerialName("latest") val latest: String? = null
    )
//
//    @Serializable
//    data class Version(
//        @SerialName("name") val name: String? = null,
//        @SerialName("version") val version: String? = null,
//        @SerialName("description") val description: String? = null,
//        @SerialName("keywords") val keywords: List<String?>? = null,
//        @SerialName("author") val author: Author? = null,
//        @SerialName("repository") val repository: Repository? = null,
//        @SerialName("main") val main: String? = null,
//        @SerialName("dependencies") val dependencies: Dependencies? = null,
//        @SerialName("devDependencies") val devDependencies: DevDependencies? = null,
//        @SerialName("engines") val engines: Engines? = null,
//        @SerialName("_npmJsonOpts") val npmJsonOpts: NpmJsonOpts? = null,
//        @SerialName("_id") val id: String? = null,
//        @SerialName("_engineSupported") val engineSupported: Boolean? = null,
//        @SerialName("_npmVersion") val npmVersion: String? = null,
//        @SerialName("_nodeVersion") val nodeVersion: String? = null,
//        @SerialName("_defaultsLoaded") val defaultsLoaded: Boolean? = null,
//        @SerialName("dist") val dist: Dist? = null,
//        @SerialName("maintainers") val maintainers: List<NpmPackageInfo.Maintainer?>? = null
//    ) {
//        @Serializable
//        data class Author(
//            @SerialName("name") val name: String? = null,
//            @SerialName("email") val email: String? = null
//        )
//
//        @Serializable
//        data class Repository(
//            @SerialName("type") val type: String? = null,
//            @SerialName("url") val url: String? = null
//        )
//
//        @Serializable
//        data class Dependencies(
//            @SerialName("validator") val validator: String? = null,
//            @SerialName("async") val async: String? = null
//        )
//
//        @Serializable
//        data class Engines(
//            @SerialName("node") val node: String? = null
//        )
//
//        @Serializable
//        data class NpmJsonOpts(
//            @SerialName("file") val `file`: String? = null,
//            @SerialName("wscript") val wscript: Boolean? = null,
//            @SerialName("contributors") val contributors: Boolean? = null,
//            @SerialName("serverjs") val serverjs: Boolean? = null
//        )
//
//        @Serializable
//        class DevDependencies
//
//        @Serializable
//        data class Dist(
//            @SerialName("shasum") val shasum: String? = null,
//            @SerialName("tarball") val tarball: String? = null,
//            @SerialName("integrity") val integrity: String? = null,
//            @SerialName("signatures") val signatures: List<Signature?>? = null
//        ) {
//            @Serializable
//            data class Signature(
//                @SerialName("keyid") val keyid: String? = null,
//                @SerialName("sig") val sig: String? = null
//            )
//        }
//    }

    @Serializable
    data class Author(
        @SerialName("name") val name: String? = null,
        @SerialName("email") val email: String? = null
    )

    @Serializable
    data class Repository(
        @SerialName("type") val type: String? = null,
        @SerialName("url") val url: String? = null
    )
}
package com.pixelperfect.packagejson.data.npm

data class LatestVersion(
    val version: String,
    val isCurrent: Boolean
)
package com.pixelperfect.packagejson.data.npm

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

interface NpmPackageInfo {
    val name: String
    val description: String
    val version: String
    val maintainers: List<Maintainer>

    @Serializable
    data class Maintainer(
        @SerialName("username") val username: String? = null,
        @SerialName("email") val email: String? = null
    )
}
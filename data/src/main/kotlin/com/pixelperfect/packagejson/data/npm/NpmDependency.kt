package com.pixelperfect.packagejson.data.npm

data class NpmDependency(
    val name: String,
    val version: String,
    val dependencies: List<NpmDependency> = emptyList()
) {
    val key = "$name:${version.cleanVersion()}"
}
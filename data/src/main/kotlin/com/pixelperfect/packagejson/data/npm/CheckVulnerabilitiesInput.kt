package com.pixelperfect.packagejson.data.npm

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class CheckVulnerabilitiesInput(
    @SerialName("name") val name: String,
    @SerialName("version") val version: String,
    @SerialName("requires") val requires: Map<String, String>,
    @SerialName("dependencies") val dependencies: Map<String, Dependency>,
) {

    companion object {
        fun create(npmPackage: NpmPackage): CheckVulnerabilitiesInput {
//            val dependencies = npmPackage.dependencies?.mapKeys { it.key.replace("@nuxt/", "") } ?: emptyMap()
            val dependencies = npmPackage.dependencies ?: emptyMap()
            return CheckVulnerabilitiesInput(
                name = npmPackage.name ?: "",
                version = npmPackage.version ?: "",
                requires = dependencies,
                dependencies = dependencies.mapValues { Dependency(it.value) }
            )
        }
    }

    @Serializable data class Dependency(
        @SerialName("version") val version: String
    )
}
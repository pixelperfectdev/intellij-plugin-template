package com.pixelperfect.packagejson.data

sealed class NotificationEvent {

    object None: NotificationEvent()

    data class Dialog(val fileType: FileType): NotificationEvent()

    data class Banner(val title: String, val message: String, val type: BannerType) : NotificationEvent()

    enum class FileType {
        File,
        Folder
    }

    enum class BannerType {
        Success,
        Error
    }
}
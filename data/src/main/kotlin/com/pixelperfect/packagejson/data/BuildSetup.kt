package com.pixelperfect.packagejson.data

interface BuildSetup {

    val APP_ID: String
    val APP_NAME: String
    val APP_VERSION: String
    val DEBUG: Boolean

    // Analytics
    val AMPLITUDE_API_KEY: String
    val BUGSNAG_KEY: String

}
package com.pixelperfect.packagejson.data.npm

import com.pixelperfect.packagejson.data.npm.Severity.Companion.toSeverity

data class NpmPackageVulnerabilities(
    val vulnerabilities: List<VulnerabilityInfo>
) {

    val hasVulnerabilities get() = vulnerabilities.isNotEmpty()
    val title: String get() {
        val low = vulnerabilities.count { it.severity == Severity.Low }.takeIf { it > 0 }?.let { "$it low" }
        val moderate = vulnerabilities.count { it.severity == Severity.Moderate }.takeIf { it > 0 }?.let { "$it moderate" }
        val high = vulnerabilities.count { it.severity == Severity.High }.takeIf { it > 0 }?.let { "$it high" }
        val critical = vulnerabilities.count { it.severity == Severity.Critical }.takeIf { it > 0 }?.let { "$it critical" }
        val count = listOf(low, moderate, high, critical).filterNotNull().joinToString()
        return vulnerabilities.takeIf { vulnerabilities.size == 1 }
            ?.firstOrNull()?.title
            ?: "Vulnerabilities found in subdependencies ($count)"
    }

    val recommendation: String get() {
        return vulnerabilities.maxBy { it.patchedVersions }.recommendation
    }

    val patchedVersions: String get() {
        return vulnerabilities.maxBy { it.patchedVersions }.patchedVersions
    }

    companion object {
        fun create(packageVulnerabilities: NpmAuditResponse): NpmPackageVulnerabilities {
            return NpmPackageVulnerabilities(
                vulnerabilities = packageVulnerabilities.advisories
                    ?.values
                    ?.mapNotNull {
                        VulnerabilityInfo(
                            url = it.url ?: return@mapNotNull null,
                            title = it.title ?: return@mapNotNull null,
                            moduleName = it.moduleName ?: return@mapNotNull null,
                            overview = it.overview ?: return@mapNotNull null,
                            recommendation = it.recommendation ?: return@mapNotNull null,
                            severity = it.severity.toSeverity() ?: return@mapNotNull null,
                            vulnerableVersions = it.vulnerableVersions ?: return@mapNotNull null,
                            patchedVersions = it.patchedVersions ?: return@mapNotNull null,
                        )
                    }
                    ?: emptyList()
            )
        }

        fun create(packageVulnerabilities: NpmVulnerabilityBulkResponse): NpmPackageVulnerabilities {
            return NpmPackageVulnerabilities(
                vulnerabilities = packageVulnerabilities.mapNotNull { (packageName, vulnerabilities) ->
                    vulnerabilities.mapNotNull { vulnerability ->
                        VulnerabilityInfo(
                            url = vulnerability.url ?: return@mapNotNull null,
                            title = vulnerability.title ?: return@mapNotNull null,
                            moduleName = packageName ?: return@mapNotNull null,
                            overview = "",
                            recommendation = "",
                            severity = vulnerability.severity.toSeverity() ?: return@mapNotNull null,
                            vulnerableVersions = vulnerability.vulnerableVersions ?: return@mapNotNull null,
                            patchedVersions = "",
                        )
                    }
                }.flatten()
            )
        }
    }
}

data class VulnerabilityInfo(
    val url: String,
    val title: String,
    val moduleName: String,
    val overview: String,
    val recommendation: String,
    val severity: Severity,
    val vulnerableVersions: String,
    val patchedVersions: String,
)

enum class Severity {
    Low,
    Moderate,
    High,
    Critical;

    companion object {
        fun String?.toSeverity(): Severity? {
            return when (this?.lowercase()) {
                "low" -> Low
                "moderate" -> Moderate
                "high" -> High
                "critical" -> Critical
                else -> null
            }
        }
    }
}
package com.pixelperfect.packagejson.data.error

class PackageNotFound: RuntimeException()
plugins {
    id("multiplatform-setup")
}

dependencies {
    implementation(Deps.Kotlin.serialization)
    implementation(project(":repository"))
}
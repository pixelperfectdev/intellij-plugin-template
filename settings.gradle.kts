rootProject.name = "intellij-package-json"

pluginManagement {
    val gradleIntellijPlugin: String? by settings
    val jetbrainsChangelogVersion: String? by settings
    val jetbrainsQodanaVersion: String? by settings
    plugins {
        id("org.jetbrains.intellij") version "$gradleIntellijPlugin"
        id("org.jetbrains.changelog") version "$jetbrainsChangelogVersion"
        id("org.jetbrains.qodana") version "$jetbrainsQodanaVersion"
    }
}

include("app")
include("common")
include("data")
include("design-system")
include("domain")
include("interactor")
include("repository")
include("service")
include("navigation")
include("translation")

include("feature:npmlist")

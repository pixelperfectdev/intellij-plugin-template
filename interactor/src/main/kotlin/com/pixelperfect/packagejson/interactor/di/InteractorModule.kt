package com.pixelperfect.packagejson.interactor.di

import com.pixelperfect.packagejson.domain.usecase.session.SessionUseCase
import com.pixelperfect.packagejson.interactor.session.SessionInteractor
import org.koin.dsl.module

val interactorModule = module {

    factory<SessionUseCase> {
        SessionInteractor()
    }

}
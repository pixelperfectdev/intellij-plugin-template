package com.pixelperfect.packagejson.interactor.session

import chrome.sendMessageToBackground
import com.pixelperfect.packagejson.data.Action
import com.pixelperfect.packagejson.data.Command
import com.pixelperfect.packagejson.domain.presenter.ContentPresenter
import com.pixelperfect.packagejson.domain.usecase.session.CommandUseCase

class CommandInteractor(
    private val presenter: ContentPresenter
): CommandUseCase {

    override suspend fun close() {
        sendMessageToBackground(message = Command(action = Action.CLOSE_CONTENT_VIEW))
    }

    override suspend fun openSettings() {
        sendMessageToBackground(message = Command(action = Action.OPEN_OPTIONS))
    }
}
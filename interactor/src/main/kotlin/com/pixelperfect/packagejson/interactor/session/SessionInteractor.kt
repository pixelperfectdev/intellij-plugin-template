package com.pixelperfect.packagejson.interactor.session

import com.pixelperfect.packagejson.common.Constants
import com.pixelperfect.packagejson.domain.usecase.session.SessionUseCase
import kotlinx.browser.document

class SessionInteractor(
): SessionUseCase {

    override suspend fun stop() {
        document.getElementById(Constants.CONTENT_VIEW_ID)?.remove()
    }

}
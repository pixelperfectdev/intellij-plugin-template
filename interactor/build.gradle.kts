import org.gradle.kotlin.dsl.sourceSets

plugins {
    id("multiplatform-setup")
    kotlin("plugin.serialization")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":translation"))
}